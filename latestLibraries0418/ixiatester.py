#
"""
This module is methods for Ixia tester, while will be used for BBE feature
"""
import ipaddress
import re


def add_string_mv(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    string_name:            string name
    :return:                dictionary
    """
    mv_args = dict()
    mv_args['pattern'] = "string"
    mv_args['string_pattern'] = kwargs['string_name']
    result = rt_handle.invoke('multivalue_config', **mv_args)
    return result


def reboot_port(rt_handle, **kwargs):
    """
    reboot port cpu
    :param rt_handle:
    :param port_list:            the tester port_list e.g. ['1/1', '1/2']
    :return:                    dictionary (status)
    """
    port_handle_list = []
    port_list = kwargs['port_list']
    if isinstance(port_list, str):
        ports = []
        ports.append(port_list)
        port_list = ports
    for port in port_list:
        port_handle = rt_handle.port_to_handle_map[port]
        port_handle_list.append(port_handle)
    result = rt_handle.invoke('reboot_port_cpu', port_list=port_handle_list)
    if result['status'] == '1':
        return rt_handle.invoke('interface_config', port_handle=port_handle_list, phy_mode='fiber')
    return result


def bbe_initialize(rt_handle):
    """
    :param rt_handle:                RT object
    :return:
    """
    rt_handle.handles = {}
    rt_handle.bgp_handle = []
    rt_handle.dhcpv4_client_handle = []
    rt_handle.dhcpv6_client_handle = []
    rt_handle.ldra_handle = []
    rt_handle.pppox_client_handle = []
    rt_handle.device_group_handle = []
    rt_handle.dhcpv4_server_handle = []
    rt_handle.dhcpv6_server_handle = []
    rt_handle.ancp_handle = []
    rt_handle.ancp_line_handle = []
    rt_handle.lac_handle = []
    rt_handle.lns_handle = []
    rt_handle.l2tp_client_handle = []
    rt_handle.l2tp_server_session_handle = []
    rt_handle.link_ip_handle = []
    rt_handle.link_ipv6_handle = []
    rt_handle.link_gre_handle = []
    rt_handle.remote_id = {}
    rt_handle.circuit_id = {}
    rt_handle.interface_id = {}
    rt_handle.v6_remote_id = {}
    rt_handle.enterprise_id = {}
    rt_handle.ae = {}
    # rt_handle.dhcpv4_index = 0
    # rt_handle.dhcpv6_index = 0
    # rt_handle.pppoev4_index = 0
    # rt_handle.pppoev6_index = 0
    rt_handle.traffic_item = []
    rt_handle.stream_id = []
    rt_handle.igmp_handles = {}
    rt_handle.igmp_handle_to_group = {}
    rt_handle.mld_handles = {}
    rt_handle.mld_handle_to_group = {}


def add_topology(rt_handle, **kwargs):
    """
    create topoloogy for ports
    :param rt_handle:
    :param kwargs:
    port_list:                              a list of ports
    :return:                                dictionary of status and topology handle
    """
    rt_handle.topology = []

    if isinstance(kwargs['port_list'], list):
        port_list = kwargs['port_list']
    else:
        raise Exception("port_list argument should be a list")
    port_handle = []
    for port in port_list:
        port_handle.append(rt_handle.port_to_handle_map[port])
    topology_status = rt_handle.invoke('topology_config', port_handle=port_handle)

    for port in port_list:
        if topology_status['topology_handle'] not in rt_handle.topology:
            rt_handle.topology.append(topology_status['topology_handle'])
        rt_handle.handles[port] = {}
        rt_handle.handles[port]['topo'] = topology_status['topology_handle']
        rt_handle.handles[port]['device_group_handle'] = []
        rt_handle.handles[port]['ethernet_handle'] = []
        rt_handle.handles[port]['ipv4_handle'] = []
        rt_handle.handles[port]['ipv6_handle'] = []
        rt_handle.handles[port]['dhcpv4_client_handle'] = []
        rt_handle.handles[port]['dhcpv6_client_handle'] = []
        rt_handle.handles[port]['pppox_client_handle'] = []
        rt_handle.handles[port]['port_handle'] = rt_handle.port_to_handle_map[port]
        rt_handle.handles[port]['dhcpv6_over_pppox_handle'] = {}
    return topology_status


def set_protocol_stacking_mode(rt_handle, **kwargs):
    """
    :param rt_handle:                    RT object
    :param mode:                    parallel or sequential, default is sequential
    :return:
    """
    mode = kwargs.get('mode', 'parallel')
    return rt_handle.invoke('topology_config', mode='modify', protocol_stacking_mode=mode, topology_handle=rt_handle.topology[0])


def __set_custom_pattern(rt_handle, **kwargs):
    """
    custom pattern for create vlan/svlan
    :param rt_handle:            RT object
    :param kwargs:
    start:                  vlan start num
    step:                   vlan step num
    repeat:                 vlan repeat num
    count:                  vlan squence length
    :return:
    multivalue:             multivalue handle for the custom pattern
    """
    try:
        _result_ = rt_handle.invoke('multivalue_config', pattern="custom")

        multivalue_handle = _result_['multivalue_handle']
        mv_args = dict()
        mv_args['multivalue_handle'] = multivalue_handle
        mv_args['custom_start'] = kwargs['start']
        mv_args['custom_step'] = "0"
        _result_ = rt_handle.invoke('multivalue_config', **mv_args)
        custom_1_handle = _result_['custom_handle']
        custom_args = dict()
        custom_args['custom_handle'] = custom_1_handle
        custom_args['custom_increment_value'] = kwargs['step']
        custom_args['custom_increment_count'] = kwargs.get('count', '4094')
        _result_ = rt_handle.invoke('multivalue_config', **custom_args)

        increment_1_handle = _result_['increment_handle']
        increment_args = dict()
        increment_args['increment_handle'] = increment_1_handle
        increment_args['custom_increment_value'] = "0"
        increment_args['custom_increment_count'] = kwargs['repeat']
        _result_ = rt_handle.invoke('multivalue_config', **increment_args)
    except:
        raise Exception("failed to create custom pattern")
    #increment_2_handle = _result_['increment_handle']
    return multivalue_handle


def add_type_tlv(rt_handle, **kwargs):
    """
    create tlv type
    :param rt_handle:
    :param kwargs:
    handle:                            dhcpv6 client handle
    tlv_name:                          type name
    tlv_include_in_messages:           tlv included in which messages
    :return:                            dictionary
    """
    tlv_args = dict()
    tlv_args['handle'] = kwargs['handle']
    tlv_args['mode'] = "create_tlv"
    tlv_args['tlv_name'] = kwargs['tlv_name']
    tlv_args['tlv_is_enabled'] = "1"
    tlv_args['tlv_include_in_messages'] = kwargs['tlv_include_in_messages']
    tlv_args['tlv_enable_per_session'] = "1"
    tlv_args['type_name'] = "Type"
    tlv_args['type_is_editable'] = "0"
    tlv_args['type_is_required'] = "1"
    tlv_args['length_name'] = "Length"
    tlv_args['length_description'] = "DHCPv6 client TLV length field."
    tlv_args['length_encoding'] = "decimal"
    tlv_args['length_size'] = "2"
    tlv_args['length_value'] = "0"
    tlv_args['length_is_editable'] = "0"
    tlv_args['length_is_enabled'] = "1"
    tlv_args['disable_name_matching'] = "1"
    _result_ = rt_handle.invoke('tlv_config', **tlv_args)
    return _result_


def add_code_tlv(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    type_handle:            result from type tlv
    field_value:            field value
    :return:                dictionary
    """
    tlv_args = dict()
    tlv_args['handle'] = kwargs['type_handle']
    tlv_args['mode'] = "create_field"
    tlv_args['field_name'] = "Code"
    tlv_args['field_value'] = kwargs['field_value']
    tlv_args['field_description'] = "DHCPv6 client TLV type field."
    tlv_args['field_encoding'] = "decimal"
    tlv_args['field_size'] = "2"
    tlv_args['field_is_editable'] = "0"
    tlv_args['field_is_enabled'] = "1"
    result = rt_handle.invoke('tlv_config', **tlv_args)
    return result


def add_field_tlv(rt_handle, **kwargs):
    """
    :param rt_handle:
    value_handle:            tlv_value_handle from type tlv
    field_name:              field name
    field_value:             field value
    mode:                    optional
    :param kwargs:
    :return:                dictionary
    """
    field_param = dict()
    field_param['handle'] = kwargs['value_handle']
    field_param['field_name'] = kwargs['field_name']
    field_param['mode'] = kwargs.get('mode', 'create_field')
    field_param['field_value'] = kwargs['field_value']
    field_param['field_is_enabled'] = '1'
    field_param['field_size'] = '2'
    field_param['field_encoding'] = 'decimal'
    field_param['field_is_editable'] = '0'
    result = rt_handle.invoke('tlv_config', **field_param)
    return result

def set_v6_option(rt_handle, **kwargs):
    """
    dhcpv6 interface_id(option 17) and remote_id (option 38)
    :param rt_handle:                    RT object
    :param kwargs:
    handle:                         dhcpv6 device handle/dhcpv6 relay agent handle
    interface_id:                   interface_id string, if with ? inside it, it means the string will increase based
                                    on the start/step/repeat at that location or default
    interface_id_start:             interface_id_start num
    interface_id_step:              interface_id_step num
    interface_id_repeat:            interface_id_repeat num
    interface_id_length:            interface_id_length
    remote_id:                      remote_id string, if with ? inside it, it means the string will increase based on
                                    the start/step/repeat or default
    remote_id_start:                remote_id_start num
    remote_id_step:                 remote_id_step num
    remote_id_repeat:               remote_id_repeat num
    remote_id_length:               remote_id_length
    enterprise_id:                  enterprise_id used inside of remote id
    enterprise_id_step:             enterprise_id_step num
    subscriber_id:                  subscriber_id string, if with ? inside it, it means the string will increase based
                                    on the start/step/repeat at that location or default
    subscriber_id_start:            subscriber_id_start num
    subscriber_id_step:             subscriber_id_step num
    subscriber_id_repeat:           subscriber_id_repeat num
    subsriber_id_length:            subscriber_id_length
    option_req:                     a list of field of option_request, e.g.[6, 67]
    tlv_include_in_messages:        the tlv included in which messages, e.g. "kSolicit kRequest"
    :return:
    result:                         dictionary of status
    """
    if 'handle' in kwargs and 'relayAgent' in kwargs['handle']:
        dhcpv6_handle = kwargs['handle']
        if 'interface_id' in kwargs:
            interface_id = kwargs.get('interface_id')
            if 'interface_id_start' in kwargs:
                start = str(kwargs.get('interface_id_start', '1'))
                step = str(kwargs.get('interface_id_step', '1'))
                repeat = str(kwargs.get('interface_id_repeat', '1'))
                length = str(kwargs.get('interface_id_length', ''))

                if '?' in interface_id:
                    increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                    interface_id = interface_id.replace('?', increment)
                else:
                    interface_id = interface_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
            tlv_args = dict()
            tlv_args['handle'] = dhcpv6_handle + "/lightweightDhcp6RelayTlvProfile/tlvProfile/defaultTlv:1"
            tlv_args['mode'] = 'modify'
            tlv_args['tlv_name'] = "[18] Interface-ID"
            tlv_args['tlv_include_in_messages'] = "kRelayForw"
            tlv_args['tlv_enable_per_session'] = '1'
            tlv_args['disable_name_matching'] = '1'
            _result_ = rt_handle.invoke('tlv_config', **tlv_args)
            print(_result_)
            _result_ = add_string_mv(rt_handle, string_name=interface_id)
            print(_result_)
            multivalue_2_handle = _result_['multivalue_handle']
            tlv2_args = dict()
            tlv_field = "/lightweightDhcp6RelayTlvProfile/tlvProfile/defaultTlv:1/value/object:1/field"
            tlv2_args['handle'] = dhcpv6_handle + tlv_field
            tlv2_args['mode'] = 'modify'
            tlv2_args['field_name'] = "Interface-ID"
            tlv2_args['field_encoding'] = 'string'
            tlv2_args['field_size'] = "0"
            tlv2_args['field_value'] = multivalue_2_handle
            tlv2_args['field_is_enabled'] = "1"
            tlv2_args['field_is_editable'] = "1"
            result = rt_handle.invoke('tlv_config', **tlv2_args)
            print(result)
        if 'v6_remote_id' in kwargs:
            remote_id = kwargs['v6_remote_id']
            if 'v6_remote_id_start' in kwargs:
                start = str(kwargs.get('v6_remote_id_start'))
                step = str(kwargs.get('v6_remote_id_step', '1'))
                repeat = str(kwargs.get('v6_remote_id_repeat', '1'))
                length = str(kwargs.get('v6_remote_id_length', ''))
                if '?' in remote_id:
                    increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                    remote_id = remote_id.replace('?', increment)
                else:
                    remote_id = remote_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
            tlv_args = dict()
            tlv_args['handle'] = dhcpv6_handle + "/lightweightDhcp6RelayTlvProfile/tlvProfile/defaultTlv:2"
            tlv_args['mode'] = 'modify'
            tlv_args['tlv_name'] = "[37] Relay Agent Remote-ID"
            tlv_args['tlv_include_in_messages'] = "kRelayForw"
            tlv_args['tlv_enable_per_session'] = '1'
            tlv_args['disable_name_matching'] = '1'
            tlv_args['tlv_is_enabled'] = "1"
            _result_ = rt_handle.invoke('tlv_config', **tlv_args)

            _result_ = add_string_mv(rt_handle, string_name=remote_id)
            print(_result_)
            multivalue_3_handle = _result_['multivalue_handle']
            tlv2_args = dict()
            tlv2_field = "/lightweightDhcp6RelayTlvProfile/tlvProfile/defaultTlv:2/value/object:2/field"
            tlv2_args['handle'] = dhcpv6_handle + tlv2_field
            tlv2_args['mode'] = 'modify'
            tlv2_args['field_name'] = "Remote-ID String"
            tlv2_args['field_encoding'] = 'string'
            tlv2_args['field_size'] = "0"
            tlv2_args['field_value'] = multivalue_3_handle
            tlv2_args['field_is_enabled'] = "1"
            tlv2_args['field_is_editable'] = "1"
            result = rt_handle.invoke('tlv_config', **tlv2_args)
            print(result)
            if 'enterprise_id' in kwargs:
                mv_args = dict()
                mv_args['pattern'] = "counter"
                mv_args['counter_start'] = kwargs['enterprise_id']
                mv_args['counter_step'] = kwargs.get('enterprise_id_step', 0)
                mv_args['counter_direction'] = "increment"
                _result_ = rt_handle.invoke('multivalue_config', **mv_args)
                print(_result_)
                subtlv_obj = "/lightweightDhcp6RelayTlvProfile/tlvProfile/defaultTlv:2/value/object:1/field"
                field_args = dict()
                field_args['handle'] = dhcpv6_handle + subtlv_obj
                field_args['mode'] = "modify"
                field_args['field_name'] = "Enterprise number"
                field_args['field_encoding'] = "decimal"
                field_args['field_size'] = "4"
                field_args['field_value'] = _result_['multivalue_handle']
                field_args['field_is_enabled'] = "1"
                field_args['field_is_editable'] = "1"
                result = rt_handle.invoke('tlv_config', **field_args)
                print(result)
        if 'subscriber_id' in kwargs:
            subscriber_id = kwargs.get('subscriber_id')
            if 'subscriber_id_start' in kwargs:
                start = str(kwargs.get('subscriber_id_start', '1'))
                step = str(kwargs.get('subscriber_id_step', '1'))
                repeat = str(kwargs.get('subscriber_id_repeat', '1'))
                length = str(kwargs.get('subscriber_id_length', ''))

                if '?' in subscriber_id:
                    increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                    subscriber_id = subscriber_id.replace('?', increment)
                else:
                    subscriber_id = subscriber_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
            handle = dhcpv6_handle + "/lightweightDhcp6RelayTlvProfile/tlvProfile/defaultTlv:3"
            subtlv_args = dict()
            subtlv_args['handle'] = handle
            subtlv_args['mode'] = "modify"
            subtlv_args['tlv_name'] = "[38] Relay Agent Subscriber-ID"
            subtlv_args['tlv_is_enabled'] = "1"
            subtlv_args['tlv_include_in_messages'] = "kRelayForw"
            subtlv_args['tlv_enable_per_session'] = "1"
            subtlv_args['disable_name_matching'] = "1"
            _result_ = rt_handle.invoke('tlv_config', **subtlv_args)
            print(_result_)
            _result_ = add_string_mv(rt_handle, string_name=subscriber_id)
            print(_result_)
            field_handle = "/lightweightDhcp6RelayTlvProfile/tlvProfile/defaultTlv:3/value/object:1/field"
            field_args = dict()
            field_args['handle'] = dhcpv6_handle + field_handle
            field_args['mode'] = "modify"
            field_args['field_name'] = "Subscriber-ID"
            field_args['field_description'] = "Relay agent subscriber ID."
            field_args['field_encoding'] = "string"
            field_args['field_size'] = "0"
            field_args['field_value'] = _result_['multivalue_handle']
            field_args['field_is_enabled'] = "1"
            field_args['field_is_editable'] = "1"
            result = rt_handle.invoke('tlv_config', **field_args)
            print(result)
    if 'handle' in kwargs and 'relayAgent' not in kwargs['handle']:
        v6option_args = dict()
        v6option_args['handle'] = kwargs['handle']
        tlv_in_message = kwargs.get('tlv_include_in_messages',
                                    "kSolicit kRequest kInformReq kRelease kRenew kRebind")
        v6option_args['tlv_include_in_messages'] = tlv_in_message
        if 'option_req' in kwargs:
            v6option_args['tlv_name'] = "[06] Option Request"
            _result_ = add_type_tlv(rt_handle, **v6option_args)
            if _result_['status'] == '0':
                return _result_
            field_list = []
            if isinstance(kwargs['option_req'], list):
                field_list = kwargs['option_req']
            else:
                field_list.append(kwargs['option_req'])
            for field in field_list:
                v6option_args['field_name'] = "[{}]".format(field)
                v6option_args['value_handle'] = _result_['tlv_value_handle']
                v6option_args['field_value'] = field
                result = add_field_tlv(rt_handle, **v6option_args)
                if result['status'] == '0':
                    return result
            v6option_args['field_value'] = '6'
            v6option_args['type_handle'] = _result_['tlv_type_handle']
            result = add_code_tlv(rt_handle, **v6option_args)
        if 'option20' in kwargs:
            v6option_args['tlv_name'] = "[20] Reconfigure Accept"
            _result_ = add_type_tlv(rt_handle, **v6option_args)
            if _result_['status'] != '1':
                return _result_
            v6option_args['field_value'] = '20'
            v6option_args['type_handle'] = _result_['tlv_type_handle']
            v6option_args['field_value'] = '20'
            result = add_code_tlv(rt_handle, **v6option_args)
    return result


def set_option_82(rt_handle, **kwargs):
    """
    this is for dhcpv4 option 82
    :param rt_handle:                RT object
    :param kwargs:
    handle:                     dhcpv4 client handle
    circuit_id:                 circuit id string( if with ? with the circuit id, it will replace ? with the increase)
    circuit_id_start:           circuit id start num
    circuit_id_step:            circuit id step num
    circuit_id_repeat:          circuit_id_repeat_num
    circuit_id_length:          circuit_id_length
    remote_id:                  remote id string
    remote_id_start:            remote id start num
    remote_id_step:             remote_id_step num
    remote_id_repeat:           remote_id_repeat num
    remote_id_length:           remote_id_length
    :return:
    status:                     1 or 0
    """
    if 'handle' in kwargs and 'v4' in kwargs.get('handle'):
        handle = kwargs.get('handle')
        if 'circuit_id' in kwargs:
            circuit_id = kwargs.get('circuit_id')
            if 'circuit_id_start' in kwargs:
                start = str(kwargs.get('circuit_id_start'))
                step = str(kwargs.get('circuit_id_step', '1'))
                repeat = str(kwargs.get('circuit_id_repeat', '1'))
                length = str(kwargs.get('circuit_id_length', ''))
                if '?' in circuit_id:
                    increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                    circuit_id = circuit_id.replace('?', increment)
                else:
                    circuit_id = circuit_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'

        if 'remote_id' in kwargs:
            remote_id = kwargs.get('remote_id')
            if 'remote_id_start' in kwargs:
                start = str(kwargs.get('remote_id_start'))
                step = str(kwargs.get('remote_id_step', '1'))
                repeat = str(kwargs.get('remote_id_repeat', '1'))
                length = str(kwargs.get('remote_id_length', ''))
                if '?' in remote_id:
                    increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                    remote_id = remote_id.replace('?', increment)
                else:
                    remote_id = remote_id + '{Inc:' + start + ',' + step + ',' +  length + ',' + repeat + '}'
        if handle not in rt_handle.circuit_id and handle not in rt_handle.remote_id:
            ##create option82 TLV
            if 'circuit_id' in kwargs or 'remote_id' in kwargs:
                tlv_args = dict()
                tlv_args['handle'] = kwargs['handle']
                tlv_args['mode'] = "create_tlv"
                tlv_args['tlv_name'] = "[82] DHCP Relay Agent Information"
                tlv_args['tlv_is_enabled'] = "1"
                tlv_args['tlv_include_in_messages'] = "kDiscover kRequest"
                tlv_args['tlv_enable_per_session'] = "1"
                tlv_args['type_name'] = "Type"
                tlv_args['type_is_editable'] = "0"
                tlv_args['type_is_required'] = "1"
                tlv_args['length_name'] = "Length"
                tlv_args['length_encoding'] = "decimal"
                tlv_args['length_size'] = "1"
                tlv_args['length_value'] = "0"
                tlv_args['length_is_editable'] = "0"
                tlv_args['length_is_enabled'] = "1"
                tlv_args['disable_name_matching'] = "1"
                _result_ = rt_handle.invoke('tlv_config', **tlv_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 tlv")
                # tlv_1_handle = _result_['tlv_handle']
                value_1_handle = _result_['tlv_value_handle']
                # length_1_handle = _result_['tlv_length_handle']
                field_args = dict()
                field_args['handle'] = _result_['tlv_type_handle']
                field_args['mode'] = "create_field"
                field_args['field_name'] = "Code"
                field_args['field_description'] = "Dhcp client TLV type field."
                field_args['field_encoding'] = "decimal"
                field_args['field_size'] = "1"
                field_args['field_value'] = "82"
                field_args['field_is_enabled'] = "1"
                field_args['field_is_editable'] = "1"
                _result_ = rt_handle.invoke('tlv_config', **field_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 tlv type field")

            if 'circuit_id' in kwargs:
                type_args = dict()
                type_args['handle'] = value_1_handle
                type_args['mode'] = "create_tlv"
                type_args['tlv_name'] = "[1] Agent Circuit ID"
                type_args['tlv_is_enabled'] = "1"
                type_args['tlv_enable_per_session'] = "1"
                type_args['type_name'] = "Type"
                type_args['type_is_editable'] = "0"
                type_args['type_is_required'] = "1"
                type_args['length_name'] = "Length"
                type_args['length_encoding'] = "decimal"
                type_args['length_size'] = "1"
                type_args['length_value'] = "0"
                type_args['length_is_editable'] = "0"
                type_args['length_is_enabled'] = "1"
                type_args['disable_name_matching'] = "1"
                _result_ = rt_handle.invoke('tlv_config', **type_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 circuitid tlv")
                #subTlv_1_handle = _result_['subtlv_handle']
                value_2_handle = _result_['tlv_value_handle']
                #length_2_handle = _result_['tlv_length_handle']
                type_2_handle = _result_['tlv_type_handle']
                _result_ = add_string_mv(rt_handle, string_name=circuit_id)
                multivalue_4_handle = _result_['multivalue_handle']
                field_args = dict()
                field_args['handle'] = value_2_handle
                field_args['mode'] = "create_field"
                field_args['field_name'] = "Circuit ID"
                field_args['field_encoding'] = "string"
                field_args['field_size'] = "0"
                field_args['field_value'] = multivalue_4_handle
                field_args['field_is_enabled'] = "1"
                field_args['field_is_editable'] = "1"
                _result_ = rt_handle.invoke('tlv_config', **field_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 circuitid field")
                rt_handle.circuit_id[handle] = _result_['tlv_field_handle']
                field_args = dict()
                field_args['handle'] = type_2_handle
                field_args['mode'] = "create_field"
                field_args['field_name'] = "Code"
                field_args['field_encoding'] = "decimal"
                field_args['field_description'] = "Dhcp client TLV type field."
                field_args['field_size'] = "1"
                field_args['field_value'] = "1"
                field_args['field_is_enabled'] = "1"
                field_args['field_is_editable'] = "0"
                _result_ = rt_handle.invoke('tlv_config', **field_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 tlv circuitid type field")

            if 'remote_id' in kwargs:
                type_args = dict()
                type_args['handle'] = value_1_handle
                type_args['mode'] = "create_tlv"
                type_args['tlv_name'] = "[2] Agent Remote ID"
                type_args['tlv_is_enabled'] = "1"
                type_args['tlv_enable_per_session'] = "1"
                type_args['type_name'] = "Type"
                type_args['type_is_editable'] = "0"
                type_args['type_is_required'] = "1"
                type_args['length_name'] = "Length"
                type_args['length_encoding'] = "decimal"
                type_args['length_size'] = "1"
                type_args['length_value'] = "0"
                type_args['length_is_editable'] = "0"
                type_args['length_is_enabled'] = "1"
                type_args['disable_name_matching'] = "1"
                _result_ = rt_handle.invoke('tlv_config', **type_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 remoteid tlv")
                #subTlv_2_handle = _result_['subtlv_handle']
                value_3_handle = _result_['tlv_value_handle']
                #length_3_handle = _result_['tlv_length_handle']
                type_3_handle = _result_['tlv_type_handle']
                _result_ = add_string_mv(rt_handle, string_name=remote_id)
                multivalue_5_handle = _result_['multivalue_handle']
                field_args = dict()
                field_args['handle'] = value_3_handle
                field_args['mode'] = "create_field"
                field_args['field_name'] = "Remote ID"
                field_args['field_encoding'] = "string"
                field_args['field_size'] = "0"
                field_args['field_value'] = multivalue_5_handle
                field_args['field_is_enabled'] = "1"
                field_args['field_is_editable'] = "1"
                _result_ = rt_handle.invoke('tlv_config', **field_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 remote id field")
                rt_handle.remote_id[handle] = _result_['tlv_field_handle']
                field_args = dict()
                field_args['handle'] = type_3_handle
                field_args['mode'] = "create_field"
                field_args['field_name'] = "Code"
                field_args['field_encoding'] = "decimal"
                field_args['field_description'] = "Dhcp client TLV type field."
                field_args['field_size'] = "1"
                field_args['field_value'] = "2"
                field_args['field_is_enabled'] = "1"
                field_args['field_is_editable'] = "0"
                _result_ = rt_handle.invoke('tlv_config', **field_args)
                if _result_['status'] != '1':
                    raise Exception("failed to create option82 remote id type field")
            return _result_
        if handle in rt_handle.circuit_id and 'circuit_id' in kwargs:
            field_args = dict()
            field_args['handle'] = rt_handle.circuit_id[handle]
            field_args['mode'] = "modify"
            field_args['field_name'] = "Circuit ID"
            field_args['field_encoding'] = "string"
            field_args['field_size'] = "0"
            field_args['field_value'] = circuit_id
            field_args['field_is_enabled'] = "1"
            field_args['field_is_editable'] = "1"
            _result_ = rt_handle.invoke('tlv_config', **field_args)
            if _result_['status'] != '1':
                raise Exception("failed to modify option82 circuitid")
        if handle in rt_handle.remote_id and 'remote_id' in kwargs:
            field_args = dict()
            field_args['handle'] = rt_handle.remote_id[handle]
            field_args['mode'] = "modify"
            field_args['field_name'] = "Remote ID"
            field_args['field_encoding'] = "string"
            field_args['field_size'] = "0"
            field_args['field_value'] = circuit_id
            field_args['field_is_enabled'] = "1"
            field_args['field_is_editable'] = "1"
            _result_ = rt_handle.invoke('tlv_config', **field_args)
            print(_result_)
            if _result_['status'] != '1':
                raise Exception("failed to modify option82 remote id ")
        return _result_


def set_vlan(rt_handle, **kwargs):
    """
    :param rt_handle:                RT object
    :param kwargs:
    handle                      device group handle
    vlan_start:                 the first vlan id
    vlan_step:                  vlan increase step
    vlan_repeat:                vlan repeat number
    vlan_length:                vlan sequence length
    svlan_start:                first svlan id
    svlan_step:                 svlan increase step
    svlan_repeat:               svlan repeat number
    svlan_length:               svlan sequence length
    vlan_priority:              vlan priority value
    vlan_priority_step:         vlan priority step value
    mac:                        MAC address
    mac_step:                   MAC address step
    mtu:                        MTU value
    :return:
    result
    """
    if 'handle' not in kwargs:
        raise Exception("handle is mandatory")
    vlan_args = dict()
    if 'svlan_start' in kwargs:
        vlan_args['start'] = kwargs.get('svlan_start')
        vlan_args['step'] = kwargs.get('svlan_step', '1')
        vlan_args['repeat'] = kwargs.get('svlan_repeat', '1')
        vlan_args['count'] = kwargs.get('svlan_length', '4094')
        svlan_handle = __set_custom_pattern(rt_handle, **vlan_args)
    if 'vlan_start' in kwargs:
        vlan_args['start'] = kwargs.get('vlan_start')
        vlan_args['step'] = kwargs.get('vlan_step', '1')
        vlan_args['repeat'] = kwargs.get('vlan_repeat', '1')
        vlan_args['count'] = kwargs.get('vlan_length', '4094')
        vlan_handle = __set_custom_pattern(rt_handle, **vlan_args)

    intf_args = dict()
    intf_args['protocol_handle'] = kwargs.get('handle')
    ###mtu must be set, otherwise, the API call will have problem
    intf_args['mtu'] = kwargs.get('mtu', '1500')
    if 'mac' in kwargs:
        intf_args['src_mac_addr'] = kwargs.get('mac')
        intf_args['src_mac_addr_step'] = "00:00:00:00:00:01"
    if 'mac_step' in kwargs:
        intf_args['src_mac_addr_step'] = kwargs['mac_step']
    if 'vlan_start' in kwargs:
        intf_args['vlan'] = '1'
        intf_args['vlan_id_count'] = '1'
        intf_args['vlan_id'] = vlan_handle
    if 'svlan_start' in kwargs:
        intf_args['vlan_id_count'] = '2'
        intf_args['vlan_id'] = "{},{}".format(svlan_handle, vlan_handle)
    if 'ethernet' in kwargs['handle']:
        intf_args['mode'] = 'modify'
    if 'vlan_priority' in kwargs:
        intf_args['vlan_user_priority'] = kwargs['vlan_priority']
    if 'vlan_priority_step' in kwargs:
        intf_args['vlan_user_priority_step'] = kwargs['vlan_priority_step']
    if 'svlan_priority' in kwargs:
        intf_args['vlan_user_priority'] = '{},{}'.format(kwargs['svlan_priority'], kwargs['vlan_priority'])
    if 'svlan_priority_step' in kwargs:
        intf_args['vlan_user_priority_step'] = \
            '{},{}'.format(kwargs['svlan_priority_step'], kwargs['svlan_priority_step'])
    _result_ = rt_handle.invoke('interface_config', **intf_args)
    #print(_result_)
    return _result_


def add_device_group(rt_handle, **kwargs):
    """
    :param rt_handle:            RT object
    :param kwargs:
    port_handle:            provide a port handle
    topology_handle:        provide a topology handle
    device_handle:          provide a device handle
    device_count:           provide device group multiplier
    name:                   device group name

    :return: a dictionary of status and device handle
    status                  1 or 0
    device_handle
    """
    device_args = dict()
    port_list = []
    if 'port_handle' in kwargs:
        port_handle = kwargs.get('port_handle')
        print(port_handle)
        port = rt_handle.handle_to_port_map[port_handle]
        port_list.append(port)
        if port not in rt_handle.handles:
            _result_ = add_topology(rt_handle, port_list=[port])
        topo_handle = rt_handle.handles[port]['topo']
        device_args['topology_handle'] = topo_handle
    elif 'topology_handle' in kwargs:
        device_args['topology_handle'] = kwargs['topology_handle']
        for key in rt_handle.handles.keys():
            if rt_handle.handles[key]['topo'] == kwargs['topology_handle']:
                port_list.append(key)
    elif 'device_group_handle' in kwargs:
        device_args['device_group_handle'] = kwargs['device_group_handle']
        for key in rt_handle.handles.keys():
            if kwargs['device_group_handle'] in rt_handle.handles[key]['device_group_handle']:
                port = key
                port_list.append(key)
    device_args['device_group_multiplier'] = kwargs.get('device_count', '1')
    result = dict()
    result['status'] = '1'
    if 'name' in kwargs:
        device_args['device_group_name'] = kwargs['name']
    status = rt_handle.invoke('topology_config', **device_args)
    if status['status'] != '1':
        result['status'] = '0'
        raise Exception("failed to create device group ")
    else:
        device_handle = status['device_group_handle']
        rt_handle.device_group_handle.append(device_handle)
        for port in port_list:
            rt_handle.handles[port]['device_group_handle'].append(device_handle)
        result['device_group_handle'] = device_handle
    return result


def add_ldra(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    port:
    handle:
    :return:
    """
    result = dict()
    result['status'] = '1'
    if 'port' in kwargs:
        port = kwargs.get('port')
        if port not in rt_handle.handles:
            _result_ = add_topology(rt_handle, port_list=[port])
            topo_handle = _result_['topology_handle']
        port_handle = rt_handle.port_to_handle_map[port]
        ##create devicegroup
        topo_handle = rt_handle.handles[port]['topo']
        dev_args = dict()
        dev_args['device_count'] = '1'
        if rt_handle.ae and port in rt_handle.ae:
            device_handle = rt_handle.handles[port]['device_group_handle'][0]
            dev_args['device_group_handle'] = device_handle
        else:
            dev_args['topology_handle'] = topo_handle
        _result_ = add_device_group(rt_handle, **dev_args)
        if _result_['status'] != '1':
            raise Exception("failed to add device group for ldra")
        device_handle = _result_['device_group_handle']
        result['device_group_handle'] = device_handle
        _result_ = set_vlan(rt_handle, handle=device_handle)
        if _result_['status'] != '1':
            raise Exception("failed to add ethernet for ldra")
        ethernet_handle = _result_['ethernet_handle']
        result['ethernet_handle'] = ethernet_handle
        dhcp_args = dict()
        dhcp_args['mode'] = "create_relay_agent"
        dhcp_args['handle'] = ethernet_handle
        dhcp_args['protocol_name'] = "Lightweight DHCPv6 Relay Agent 1"
        dhcp_args['dhcp_range_relay_type'] = "lightweight"
        _result_ = rt_handle.invoke('emulation_dhcp_group_config', **dhcp_args)
        if _result_['status'] != '1':
            raise Exception("failed to add lightweight dhcpv6relayagent for ldra")
        ldra_handle = _result_['dhcpv6relayagent_handle']
        result['ldra_handle'] = ldra_handle
        rt_handle.ldra_handle.append(ldra_handle)
        return result

def add_dhcp_client(rt_handle, **kwargs):
    """
    :param rt_handle:                RT object
    :param kwargs:
    mandatory:
    port:                       Tester physical port
    num_sessions:               client counts

    optional:
    ip_type:                    ipv4, ipv4, dual
    vlan_start:                 the first vlan id
    vlan_step:                  vlan increase step
    vlan_repeat:                vlan repeat number
    vlan_length:                vlan sequence length
    svlan_start:                first svlan id
    svlan_step:                 svlan increase step
    svlan_repeat:               svlan repeat number
    svlan_length:               svlan sequence length
    remote_id:                  option82 remote_id string
    remote_id_start:            remote id start number
    remote_id_step:             remote id step number
    remote_id_repeat:           remote id repeat number
    circuit_id:                 option82 circuit id string
    circuit_id_start:           circuit id start number
    circuit_id_step:            circuit id step number
    circuit_id_repeat:          circuit id repeat number
    v6_remote_id:               v6 option 38 remote id string
    v6_remote_id_start:         remote id start number
    v6_remote_id_step:          remote id step number
    v6_remote_id_repeat:        remote id repeat number
    enterprise_id:              v6 enterprise vendor id, used with remote id
    enterprise_id_step:         enterprise id increase step
    interface_id:               v6 option 17 interface id string
    interface_id_start:         v6 interface id start number
    interface_id_step:          v6 interface id step number
    interface_id_repeat:        v6 interface id repeat number
    option_req:                 v6 option request
    rapid_commit:               use_rapid_commit value 1 or 0
    dhcp4_broadcast:            dhcpv4 broadcast value 1 or 0
    dhcpv6_ia_type:             dhcpv6 IA type: IANA, IAPD, IANA_IAPD
    v6_max_no_per_client:       The maximum number of addresses/prefixes that can be negotiated by a DHCPv6 Client
    dhcpv6_iana_count:          The number of IANA IAs requested in a single negotiation
    dhcpv6_iapd_count:          The number of IAPD IAs requested in a single negotiation
    softgre:                    softgre feature 1 or 0
    gre_dst_ip:                 softgre tunnel destination address
    gre_local_ip:               softgre tunnel local address
    gre_netmask:                softgre tunnel mask
    gre_gateway:                softgre tunnel gateway address
    gre_vlan_id:                softgre vlan id (optional)
    gre_vlan_id_step:           softgre vlan id step(optional)
    gre_vlan_id_repeat:         softgre vlan id repeat(optional)
    gre_tunnel_num:             softgre tunnel number, by default is 1
    mac:                        mac start addr
    mac_step:                   mac address step


    :return: a dictionary of status and handle
    status:                     1 or 0
    device_group_handle:
    ethernet_handle:
    dhcpv4_client_handle:
    dhcpv6_client_handle:
    """
    result = dict()
    result['status'] = '1'
    dhcp_args = dict()
    ldra = 0
    if 'ip_type' in kwargs:
        if kwargs['ip_type'] == 'dual' or kwargs['ip_type'] == 'ipv6':
            if 'interface_id' in kwargs or 'v6_remote_id' in kwargs:
                ldra = 1
    if 'port' in kwargs:
        port = kwargs.get('port')
        if port not in rt_handle.handles:
            _result_ = add_topology(rt_handle, port_list=[port])
            topo_handle = _result_['topology_handle']
        port_handle = rt_handle.port_to_handle_map[port]
        ##create devicegroup
        topo_handle = rt_handle.handles[port]['topo']

        if 'softgre' in kwargs:
            gre_args = dict()
            gre_args['port'] = port
            gre_args['ip_addr'] = kwargs['gre_local_ip']
            gre_args['gateway'] = kwargs['gre_gateway']
            gre_args['gre_dst_ip'] = kwargs['gre_dst_ip']
            if 'gre_vlan_id' in kwargs:
                gre_args['vlan_start'] = kwargs['gre_vlan_id']
                gre_args['vlan_step'] = kwargs.get('gre_vlan_id_step', '1')
                gre_args['vlan_repeat'] = kwargs.get('gre_vlan_id_repeat', "1")
            if 'gre_netmask' in kwargs:
                gre_args['netmask'] = kwargs['gre_netmask']
            if 'gre_tunnel_num' in kwargs:
                gre_args['count'] = kwargs['gre_tunnel_num']
            config_status = add_link(rt_handle, **gre_args)
            device_handle = config_status['device_group_handle']
            result['gre_handle'] = config_status['gre_handle']
            topology_status = add_device_group(rt_handle,
                                               device_group_handle=device_handle,
                                               device_count=kwargs.get('num_sessions', '1'))
        elif ldra:
            params = kwargs
            if kwargs['ip_type'] == 'dual':
                params.pop('ip_type')
                _result_ = add_dhcp_client(rt_handle, **params)
                if _result_['status'] != '1':
                    raise Exception("failed to add dhcp client for v4 users when in LDRA dual mode")
                result['dhcpv4_client_handle'] = _result_['dhcpv4_client_handle']
            _result_ = add_ldra(rt_handle, **kwargs)
            if _result_['status'] != '1':
                raise Exception("failed to add ldra device")
            ldra_handle = _result_['ldra_handle']
            result['ldra_handle'] = ldra_handle
            device_handle = _result_['device_group_handle']
            kwargs['ip_type'] = 'ipv6'
            topology_status = add_device_group(rt_handle,
                                               device_group_handle=device_handle,
                                               device_count=kwargs.get('num_sessions', '1'))
        else:
            if rt_handle.ae and port in rt_handle.ae:
                device_handle = rt_handle.handles[port]['device_group_handle'][0]
                topology_status = add_device_group(rt_handle,
                                                   device_group_handle=device_handle,
                                                   device_count=kwargs.get('num_sessions', '1'))
            else:
                topology_status = add_device_group(rt_handle,
                                                   topology_handle=topo_handle,
                                                   device_count=kwargs.get('num_sessions', '1'))
        if topology_status['status'] != '1':
            result['status'] = '0'
            raise Exception("failed to create device group for port handle {}".format(port_handle))

        device_handle = topology_status['device_group_handle']
        result['device_group_handle'] = topology_status['device_group_handle']
        status = set_vlan(rt_handle, handle=device_handle, **kwargs)
        if status['status'] != '1':
            result['status'] = '0'
            raise Exception("failed to create ethernet for device group {}".format(device_handle))
        #dhcp_args = dict()
        #dhcp_args['handle'] = port_handle
        dhcp_args['handle'] = status['ethernet_handle']
        result['ethernet_handle'] = status['ethernet_handle']
        dhcp_args['mode'] = 'create'
    elif 'handle' in kwargs:
        dhcp_args['mode'] = 'modify'
        dhcp_args['handle'] = kwargs['handle']
        if 'num_sessions' in kwargs:
            dhcp_args['num_sessions'] = kwargs['num_sessions']
        if 'vlan_start' in kwargs or 'svlan_start' in kwargs or 'mac' in kwargs:
            set_vlan(rt_handle, **kwargs)

    if 'dhcpv6_ia_type' in kwargs:
        dhcp_args['dhcp6_range_ia_type'] = kwargs.get('dhcpv6_ia_type')

    if 'rapid_commit' in kwargs:
        dhcp_args['use_rapid_commit'] = kwargs.get('rapid_commit')

    if 'dhcp4_broadcast' in kwargs:
        dhcp_args['dhcp4_broadcast'] = kwargs.get('dhcp4_broadcast')

    if 'v6_max_no_per_client' in kwargs:
        dhcp_args['dhcp6_range_max_no_per_client'] = kwargs.get('v6_max_no_per_client')

    if 'dhcpv6_iana_count' in kwargs:
        dhcp_args['dhcp6_range_iana_count'] = kwargs.get('dhcpv6_iana_count')

    if 'dhcpv6_iapd_count' in kwargs:
        dhcp_args['dhcp6_range_iapd_count'] = kwargs.get('dhcpv6_iapd_count')
    v6handle = None
    handle = None
    if 'ip_type' in kwargs:
        if 'dual' in kwargs['ip_type']:
            config_status = rt_handle.invoke('emulation_dhcp_group_config', **dhcp_args)
            if config_status['status'] != '1':
                result['status'] = '0'
                raise Exception("failed to add dhcp client")
            else:
                if 'dhcpv4client_handle' in config_status:
                    handle = config_status['dhcpv4client_handle']
                    # rt_handle.dhcpv4_index += 1
                    # rt_handle.device_to_dhcpv4_index[handle] = rt_handle.dhcpv4_index
                    rt_handle.dhcpv4_client_handle.append(handle)
                    rt_handle.handles[port]['dhcpv4_client_handle'].append(handle)
                    result['dhcpv4_client_handle'] = handle

            dhcp_args['dhcp_range_ip_type'] = 'ipv6'
            config_status = rt_handle.invoke('emulation_dhcp_group_config', **dhcp_args)
            if config_status['status'] != '1':
                result['status'] = '0'
                raise Exception("failed to add dhcpv6 client for dual stack")
            else:
                if 'dhcpv6client_handle' in config_status:
                    v6handle = config_status['dhcpv6client_handle']
                    # rt_handle.dhcpv6_index += 1
                    # rt_handle.device_to_dhcpv6_index[v6handle] = rt_handle.dhcpv6_index
                    rt_handle.dhcpv6_client_handle.append(v6handle)
                    rt_handle.handles[port]['dhcpv6_client_handle'].append(v6handle)
                    result['dhcpv6_client_handle'] = v6handle

        elif kwargs['ip_type'] == "ipv4":
            dhcp_args['dhcp_range_ip_type'] = kwargs.get('ip_type')
            config_status = rt_handle.invoke('emulation_dhcp_group_config', **dhcp_args)
            if config_status['status'] != '1':
                result['status'] = '0'
                raise Exception("failed to add dhcp client")
            else:
                if 'dhcpv4client_handle' in config_status:
                    handle = config_status['dhcpv4client_handle']
                    # rt_handle.dhcpv4_index += 1
                    # rt_handle.device_to_dhcpv4_index[handle] = rt_handle.dhcpv4_index
                    rt_handle.dhcpv4_client_handle.append(handle)
                    rt_handle.handles[port]['dhcpv4_client_handle'].append(handle)
                    result['dhcpv4_client_handle'] = handle

        elif kwargs['ip_type'] == "ipv6":
            dhcp_args['dhcp_range_ip_type'] = kwargs.get('ip_type')
            config_status = rt_handle.invoke('emulation_dhcp_group_config', **dhcp_args)
            if config_status['status'] != '1':
                result['status'] = '0'
                raise Exception("failed to add dhcpv6 client")
            else:
                if 'dhcpv6client_handle' in config_status:
                    v6handle = config_status['dhcpv6client_handle']
                    # rt_handle.dhcpv6_index += 1
                    # rt_handle.device_to_dhcpv6_index[v6handle] = rt_handle.dhcpv6_index
                    rt_handle.dhcpv6_client_handle.append(v6handle)
                    rt_handle.handles[port]['dhcpv6_client_handle'].append(v6handle)
                    result['dhcpv6_client_handle'] = v6handle
    else:
        kwargs['ip_type'] = "ipv4"
        dhcp_args['dhcp_range_ip_type'] = kwargs.get('ip_type')
        config_status = rt_handle.invoke('emulation_dhcp_group_config', **dhcp_args)
        if config_status['status'] != '1':
            result['status'] = '0'
            raise Exception("failed to add dhcp client")
        else:
            if 'dhcpv4client_handle' in config_status:
                handle = config_status['dhcpv4client_handle']
                rt_handle.dhcpv4_client_handle.append(handle)
                rt_handle.handles[port]['dhcpv4_client_handle'].append(handle)
                # rt_handle.dhcpv4_index += 1
                result['dhcpv4_client_handle'] = handle
                #rt_handle.device_to_dhcpv4_index[handle] = rt_handle.dhcpv4_index
    if handle:
        if 'remote_id' in kwargs or 'circuit_id' in kwargs:
            if not set_option_82(rt_handle, handle=handle, **kwargs):
                result['status'] = '0'
    if v6handle:
        if 'interface_id' in kwargs or 'v6_remote_id' in kwargs and ldra:
            set_v6_option(rt_handle, handle=ldra_handle, **kwargs)

        if 'option_req' in kwargs or 'option20' in kwargs:
            set_v6_option(rt_handle, handle=v6handle, **kwargs)

    return result


def set_dhcp_rate(rt_handle, **kwargs):
    """
    :param rt_handle:                RT object
    :param kwargs:
    Mandatory:
     handle:                    dhcp client handle

    Optional:
    type:                       dhcpv4 or v6, can be used for setting login/logout rate only
    msg_timeout:                timeout for a msg like discover
    login_rate:                 request rate (must be a list for all the device/ports
    outstanding:                outstanding size for login(must be a list for all)
    logout_rate:                release rate(must be a list for all the device/ports)
    retry_count:                retry times for msg
    login_rate_mode:            starting scale mode (per port/per device)
    logout_rate_mode:           stop scale mode

    :return:
    result                      dictionary of status
    """
    if not ('handle' in kwargs or 'type' in kwargs):
        raise Exception("dhcp handle or type  must be provided")
    dhcp_args = dict()

    if 'login_rate' in kwargs:
        dhcp_args['start_scale_mode'] = kwargs.get('login_rate_mode', 'device_group')

        login_rate = __add_rate_multivalue(rt_handle, kwargs['login_rate'])
        dhcp_args['request_rate'] = login_rate

        dhcp_args['outstanding_session_count'] = '1000'
        dhcp_args['override_global_setup_rate'] = '1'
        if 'outstanding' in kwargs:
            dhcp_args['outstanding_session_count'] = __add_rate_multivalue(rt_handle, kwargs['outstanding'])
            ##will change to multivalue handle for the rates in the future

    if 'logout_rate' in kwargs:
        dhcp_args['stop_scale_mode'] = kwargs.get('logout_rate_mode', 'device_group')
        logout_rate = __add_rate_multivalue(rt_handle, kwargs['logout_rate'])
        dhcp_args['release_rate'] = logout_rate
        dhcp_args['outstanding_releases_count'] = '1000'
        dhcp_args['override_global_teardown_rate'] = '1'

    if 'msg_timeout' in kwargs:
        dhcp_args['msg_timeout'] = kwargs['msg_timeout']

    if 'retry_count' in kwargs:
        dhcp_args['retry_count'] = kwargs['retry_count']

    if 'dhcpv6_sol_retries' in kwargs:
        dhcp_args['dhcp6_sol_max_rc'] = kwargs['dhcpv6_sol_retries']

    if 'dhcpv6_sol_timeout' in kwargs:
        dhcp_args['dhcp6_sol_timeout'] = kwargs['dhcpv6_sol_timeout']

    if dhcp_args:
        if 'v4' in kwargs['handle']:
            dhcp_args['dhcp4_arp_gw'] = '1'
            dhcp_args['ip_version'] = '4'
        if 'v6' in kwargs['handle']:
            dhcp_args['dhcp6_ns_gw'] = '1'
            dhcp_args['ip_version'] = '6'
        dhcp_args['handle'] = "/globals"
        dhcp_args['mode'] = "create"
        config_status = rt_handle.invoke('emulation_dhcp_config', **dhcp_args)
    return config_status


def __add_rate_multivalue(rt_handle, listitem):
    """
    :param rt_handle:                RT object
    :param listitem:            a list of rate values
    :return:
    """
    if isinstance(listitem, list):
        value = ''
        index = ''
        length = len(listitem)
        seq = 0
        for i in listitem:
            seq += 1
            if seq == length:
                value += "{}".format(i)
                index += "{}".format(seq)
            else:
                value += "{},".format(i)
                index += "{},".format(seq)
        print(value)
        print(index)
        mv_args = dict()
        mv_args['pattern'] = "single_value"
        mv_args['single_value'] = "50"
        mv_args['overlay_value'] = "{}".format(value)
        mv_args['overlay_index'] = "{}".format(index)
        _result_ = rt_handle.invoke('multivalue_config', **mv_args)
        if _result_['status'] != '1':
            raise Exception("failed to create multivalue handle for listitem {}".format(listitem))
        else:
            return  _result_['multivalue_handle']
    else:
        raise Exception("login rate must be a list")


def dhcp_client_action(rt_handle, **kwargs):
    """
    :param rt_handle:        RT object
    :param kwargs:
    port_handle:        specify a port handle to login all the clients?
    handle:             dhcp handles
    action:             start_handle, stop, renew, abort_handle, restart_down
    :return:
    result              dictionary include status and log message

    #dhcp_client_action(rt_handle, handle=rt_handle.handles['1/1']['dhcpv4_client_handle'][0], action='bind')
    #dhcp_client_action(rt_handle, port_handle='1/1/1', action='bind')
    """
    result = dict()
    dhcp_args = dict()
    result['status'] = '1'
    if 'handle' not in kwargs or 'action' not in kwargs:
        print("mandatory params 'handle/action' not provided ")
        result['status'] = '0'
    else:
        dhcp_args['handle'] = kwargs['handle']
        dhcp_args['action'] = kwargs['action']
        if 'restart' in kwargs['action']:
            dhcp_args['action'] = 'restart_down'
        elif 'start' in kwargs['action']:
            dhcp_args['action'] = 'bind'
        elif 'stop' in kwargs['action']:
            dhcp_args['action'] = 'release'

        result = rt_handle.invoke('emulation_dhcp_control', **dhcp_args)
    return result


def dhcp_client_stats(rt_handle, **kwargs):
    """
    :param rt_handle:        RT object
    :param kwargs:
    port_handle:        specify a port handle to get the aggregated stats
    handle:             specify a dhcp handle to get the stats
    dhcp_version:       dhcp4/dhcp6
    execution_timeout   specify the timeout for the command
    mode:               aggregate_stats/session
    :return:            dictionary
    """
    result = rt_handle.invoke('emulation_dhcp_stats', **kwargs)
    return result


def add_pppoe_client(rt_handle, **kwargs):
    """
    :param rt_handle:               RT object
    :param kwargs:
    port:                      specify a port  for creating a simulation
    num_sessions:              specify the simulation count

    auth_req_timeout:          authentication request timeout
    echo_req:                  echo request 1 or 0
    echo_rsp:                  echo response 1 or 0
    ip_type:                    v4/dual/v6
    vlan_start:                 the first vlan id
    vlan_step:                  vlan increase step
    vlan_repeat:                vlan repeat number
    vlan_length:                vlan sequence length
    svlan_start:                first svlan id
    svlan_step:                 svlan increase step
    svlan_repeat:               svlan repeat number
    svlan_length:               svlan sequence length
    remote_id:                  agent_remote_id string, for example can be "remoteid" or "remoteid?"
    remote_id_start:            remote_id start value
    remote_id_step:             remote_id step value
    remote_id_repeat:           remote_id repeat value
    remote_id_length:           remote_id length
    circuit_id:                 agent_circuit_id string
    circuit_id_start:           circuit_id start value
    circuit_id_step:            circuit_id step value
    circuit_id_repeat:          circuit_id repeat value
    circuit_id_length:          circuit_id length
    auth_mode:                  authentication mode: pap/chap/pap_or_chap
    username:                   ppp username
    password:                   ppp password
    ipcp_req_timeout:           ipcp request timeout
    max_auth_req:               maximum authentication requests
    max_padi_req:               maximum PADI requests
    max_padr_req:               maximum PADR requests
    max_ipcp_retry:             maximum ipcp retry
    max_terminate_req:          maximum terminate requests
    echo_req_interval:          echo requests interval
    dhcpv6_ia_type:             dhcpv6 ia type: iapd/iana/iana_iapd

    :return:
    result:                     dictionary of status, pppox_client_handle, dhcpv6_client_handle
    """
    result = dict()
    result['status'] = '1'
    pppoe_args = dict()
    if 'port' in kwargs:
        port = kwargs.get('port')
        if port not in rt_handle.handles:
            _result_ = add_topology(rt_handle, port_list=[port])
        port_handle = rt_handle.port_to_handle_map[port]
        if rt_handle.ae and port in rt_handle.ae:
            device_handle = rt_handle.handles[port]['device_group_handle'][0]
            topology_status = add_device_group(rt_handle,
                                               device_group_handle=device_handle,
                                               device_count=kwargs.get('num_sessions', '1'))
        else:
        ##create devicegroup
            topo_handle = rt_handle.handles[port]['topo']
            topology_status = add_device_group(rt_handle,
                                               topology_handle=topo_handle,
                                               device_count=kwargs.get('num_sessions', '1'))
        if topology_status['status'] != '1':
            result['status'] = '0'
            raise Exception("failed to create device group for port handle {}".format(port_handle))

        device_handle = topology_status['device_group_handle']
        result['device_group_handle'] = topology_status['device_group_handle']
        status = set_vlan(rt_handle, handle=device_handle, **kwargs)
        if status['status'] != '1':
            result['status'] = '0'
            raise Exception("failed to create ethernet for device group {}".format(device_handle))

        pppoe_args['handle'] = status['ethernet_handle']
        result['ethernet_handle'] = status['ethernet_handle']
        pppoe_args['mode'] = 'add'
        pppoe_args['port_role'] = 'access'
    if 'handle' in kwargs:
        pppoe_args['handle'] = kwargs['handle']
        pppoe_args['mode'] = 'modify'
        if 'num_sessions' in kwargs:
            pppoe_args['num_sessions'] = kwargs['num_sessions']
        if 'vlan_start' in kwargs or 'svlan_start' in kwargs:
            set_vlan(rt_handle, **kwargs)
    pppoe_args['port_role'] = 'access'
    if 'auth_req_timeout' in kwargs:
        pppoe_args['auth_req_timeout'] = kwargs['auth_req_timeout']
    if 'echo_req' in kwargs:
        pppoe_args['echo_req'] = kwargs['echo_req']
    if 'echo_rsp' in kwargs:
        pppoe_args['echo_rsp'] = kwargs['echo_rsp']
    if 'ip_type' in kwargs:
        if 'v4' in kwargs['ip_type']:
            pppoe_args['ip_cp'] = 'ipv4_cp'
        if 'v6' in kwargs['ip_type']:
            pppoe_args['ip_cp'] = 'ipv6_cp'
            pppoe_args['dhcpv6_hosts_enable'] = '1'
            if 'dhcpv6_ia_type' in kwargs:
                pppoe_args['dhcp6_pd_client_range_ia_type'] = kwargs['dhcpv6_ia_type']
        if 'dual' in kwargs['ip_type']:
            pppoe_args['ip_cp'] = 'dual_stack'
            pppoe_args['dhcpv6_hosts_enable'] = '1'
            if 'dhcpv6_ia_type' in kwargs:
                pppoe_args['dhcp6_pd_client_range_ia_type'] = kwargs['dhcpv6_ia_type']

    if 'ipcp_req_timeout' in kwargs:
        pppoe_args['ipcp_req_timeout'] = kwargs['ipcp_req_timeout']
    if 'max_auth_req' in kwargs:
        pppoe_args['max_auth_req'] = kwargs['max_auth_req']
    if 'max_padi_req' in kwargs:
        pppoe_args['max_padi_req'] = kwargs['max_padi_req']
    if 'max_padr_req' in kwargs:
        pppoe_args['max_padr_req'] = kwargs['max_padr_req']
    if 'max_ipcp_retry' in kwargs:
        pppoe_args['max_ipcp_retry'] = kwargs['max_ipcp_retry']
    if 'max_terminate_req' in kwargs:
        pppoe_args['max_terminate_req'] = kwargs['max_terminate_req']
    if 'echo_req_interval' in kwargs:
        pppoe_args['echo_req_interval'] = kwargs['echo_req_interval']
    if 'auth_mode' in kwargs:
        pppoe_args['auth_mode'] = kwargs['auth_mode']
        if 'username' in kwargs:
            username = kwargs['username'].replace('?', '{Inc:1,,,1}')
            _result_ = add_string_mv(rt_handle, string_name=username)
            multivalue_5_handle = _result_['multivalue_handle']
        if 'pap' in kwargs['auth_mode']:
            if 'username'  in kwargs:
                pppoe_args['username'] = multivalue_5_handle
            if 'password' in kwargs:
                pppoe_args['password'] = kwargs['password']
        if 'chap' in kwargs['auth_mode']:
            if 'username' in kwargs:
                pppoe_args['chap_name'] = multivalue_5_handle
            if 'password' in kwargs:
                pppoe_args['chap_secret'] = kwargs['password']

    if 'circuit_id' in kwargs:
        circuit_id = kwargs.get('circuit_id')
        if 'circuit_id_start' in kwargs:
            start = str(kwargs.get('circuit_id_start'))
            step = str(kwargs.get('circuit_id_step', '1'))
            repeat = str(kwargs.get('circuit_id_repeat', '1'))
            length = str(kwargs.get('circuit_id_length', ''))
            if '?' in circuit_id:
                increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                circuit_id = circuit_id.replace('?', increment)
            else:
                circuit_id = circuit_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
        else:
            circuit_id = kwargs['circuit_id'].replace('?', '{Inc:1,,,1}')
        _result_ = add_string_mv(rt_handle, string_name=circuit_id)
        print(_result_)
        multivalue_6_handle = _result_['multivalue_handle']
        pppoe_args['agent_circuit_id'] = multivalue_6_handle

    if 'remote_id' in kwargs:
        remote_id = kwargs.get('remote_id')
        if 'remote_id_start' in kwargs:
            start = str(kwargs.get('remote_id_start'))
            step = str(kwargs.get('remote_id_step', '1'))
            repeat = str(kwargs.get('remote_id_repeat', '1'))
            length = str(kwargs.get('remote_id_length', ''))
            if '?' in remote_id:
                increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                remote_id = remote_id.replace('?', increment)
            else:
                remote_id = remote_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
        else:
            remote_id = kwargs['remote_id'].replace('?', '{Inc:1,,,1}')
        _result_ = add_string_mv(rt_handle, string_name=remote_id)
        print(_result_)
        multivalue_7_handle = _result_['multivalue_handle']
        pppoe_args['agent_remote_id'] = multivalue_7_handle
    if 'circuit_id' in kwargs or 'remote_id' in kwargs:
        pppoe_args['enable_client_signal_loop_id'] = '1'
    pppoe_args['redial'] = kwargs.get('redial', 0)
    config_status = rt_handle.invoke('pppox_config', **pppoe_args)
    #print(config_status)
    if config_status['status'] != '1':
        result['status'] = '0'
        raise Exception("failed to add/modify pppoe client for port {}".format(port))
    else:
        if 'pppox_client_handle' in config_status:
            handle = config_status['pppox_client_handle']
            rt_handle.pppox_client_handle.append(handle)
            result['pppox_client_handle'] = handle
            rt_handle.handles[port]['pppox_client_handle'].append(handle)
            if 'ip_type' in kwargs:
                if 'v6' in kwargs['ip_type'] or 'dual' in kwargs['ip_type']:
                    v6handle = config_status['dhcpv6_client_handle']
                    rt_handle.dhcpv6_client_handle.append(v6handle)
                    result['dhcpv6_client_handle'] = v6handle
                    rt_handle.handles[port]['dhcpv6_over_pppox_handle'][handle] = v6handle

    return result


def set_pppoe_rate(rt_handle, **kwargs):
    """
    :param rt_handle:                RT object
    :param kwargs:
    handle:                     specify a pppox handle
    login_rate:                 login rate
    outstanding:                outstanding size for login
    logout_rate:                logout rate
    :return:
    """
    if not ('login_rate' in kwargs or 'logout_rate' in kwargs):
        status = rt_handle.invoke('pppox_config', mode='modify', **kwargs)
        return status

    else:
    ##set the login/logout rate
        pppox_args = dict()
        pppox_args['port_role'] = "access"
        pppox_args['handle'] = "/globals"
        pppox_args['mode'] = 'add'
        if 'login_rate' in kwargs:
            login_rate = __add_rate_multivalue(rt_handle, kwargs['login_rate'])
            pppox_args['attempt_rate'] = login_rate
            pppox_args['attempt_max_outstanding'] = '1000'
            pppox_args['attempt_scale_mode'] = kwargs.get('rate_mode', 'device_group')
            if 'outstanding' in kwargs:
                pppox_args['attempt_max_outstanding'] = __add_rate_multivalue(rt_handle, kwargs['outstanding'])
        if 'logout_rate' in kwargs:
            logout_rate = __add_rate_multivalue(rt_handle, kwargs['logout_rate'])
            pppox_args['disconnect_rate'] = logout_rate
            pppox_args['disconnect_max_outstanding'] = '1000'
            pppox_args['disconnect_scale_mode'] = kwargs.get('rate_mode', 'device_group')

        result = rt_handle.invoke('pppox_config', **pppox_args)
        return result


def pppoe_client_action(rt_handle, **kwargs):
    """
    login/logout pppoe client
    :param rt_handle:            RT object
    :param kwargs:
    handle:                 pppox handles/ dhcpv6 over pppox handle
    action:                 start_handle, stop, restart_down, reset, abort
    :return:
    status
    """
    result = dict()
    result['status'] = '1'
    pppoe_args = dict()

    if 'action' not in kwargs or 'handle' not in kwargs:
        print("mandatory params 'handle/action' not provided ")
        result['status'] = '0'
    else:


        pppoe_args['action'] = kwargs.get('action')
        if 'restart' in kwargs['action']:
            pppoe_args['action'] = 'restart_down'
        elif 'start' in kwargs['action']:
            pppoe_args['action'] = 'connect'
        elif 'stop' in kwargs['action']:
            match = re.match(r'.*pppoxclient:\d+', kwargs['handle'])
            kwargs['handle'] = match.group(0)
            pppoe_args['action'] = 'disconnect'

        pppoe_args['handle'] = kwargs['handle']
        result = rt_handle.invoke('pppox_control', **pppoe_args)
    return result


def pppoe_client_stats(rt_handle, **kwargs):
    """
    get statistics for pppoe client
    :param rt_handle:                RT object
    :param kwargs:
    port_handle:                port handle used to retrieve the statistics
    handle:                     pppox handle used to retrieve the statistics
    mode:                       aggregate /session /session all
    execution_timeout:          the execution timeout setting, default is 1800
    :return:
    status
    """
    status = rt_handle.invoke('pppox_stats', **kwargs)
    return status


def add_link(rt_handle, **kwargs):
    """
    :param rt_handle:                RT object
    :param kwargs:
    port                        physical tester port
    vlan_start:                 the first vlan id
    vlan_step:                  vlan increase step
    vlan_repeat:                vlan repeat number
    vlan_length:                vlan sequence length
    svlan_start:                first svlan id
    svlan_step:                 svlan increase step
    svlan_repeat:               svlan repeat number
    svlan_length:               svlan sequence length
    ip_addr
    ip_addr_step
    netmask:                    netmask, e.g. 255.255.255.0
    gateway
    ipv6_addr
    ipv6_addr_step
    ipv6_prefix_length
    ipv6_gateway
    gateway_mac:                gateway mac address
    gre_dst_ip
    gre_dst_ip_step
    count
    mac_resolve:               resolve gateway
    name:                      link name

    :return:
    device group handle
    ethernet handle
    ip handle
    gre handle
    status
    """
    if 'port' not in kwargs:
        raise Exception("port is mandatory when adding link device")
    port = kwargs.get('port')
    #port_handle = rt_handle.port_to_handle_map[port]
    if port not in rt_handle.handles:
        _result_ = add_topology(rt_handle, port_list=[port])
    topo_handle = rt_handle.handles[port]['topo']
    count = kwargs.get('count', '1')
    link_args = dict()
    result = dict()
    result['status'] = '1'
    device_args = {}
    if 'name' in kwargs:
        device_args['device_group_name'] = kwargs['name']
    if rt_handle.ae and port in rt_handle.ae:
        device_handle = rt_handle.handles[port]['device_group_handle'][0]
        device_args['device_group_handle'] = device_handle
    else:
        device_args['topology_handle'] = topo_handle
    device_args['device_count'] = count
    #create device group first
    status_config = add_device_group(rt_handle, **device_args)
    if status_config['status'] != '1':
        result['status'] = '0'
        raise Exception("failed to add device group for port {}".format(port))
    else:
        device_group_handle = status_config['device_group_handle']
        link_args['protocol_handle'] = device_group_handle
        rt_handle.device_group_handle.append(device_group_handle)
        rt_handle.handles[port]['device_group_handle'].append(device_group_handle)
        result['device_group_handle'] = device_group_handle

    # create ethernet and set vlans
    status_config = set_vlan(rt_handle, handle=device_group_handle, **kwargs)
    if status_config['status'] != '1':
        result['status'] = '0'
        raise Exception("failed to add ethernet/vlan for device group {}".format(device_group_handle))
    else:
        ethernet_handle = status_config['ethernet_handle']
        rt_handle.handles[port]['ethernet_handle'].append(ethernet_handle)
        result['ethernet_handle'] = ethernet_handle

    intf_args = dict()
    intf_args['protocol_handle'] = ethernet_handle
    if 'ip_addr' in kwargs:
        intf_args['intf_ip_addr'] = kwargs['ip_addr']
        intf_args['intf_ip_addr_step'] = kwargs.get('ip_addr_step', '0.0.1.0')
        if 'gateway' in kwargs:
            intf_args['gateway'] = kwargs['gateway']
            intf_args['gateway_step'] = kwargs.get('ip_addr_step', '0.0.1.0')
        if 'netmask' in kwargs:
            intf_args['netmask'] = kwargs['netmask']
        if 'mac_resolve' in kwargs:
            intf_args['ipv4_resolve_gateway'] = kwargs['mac_resolve']
        if 'gateway_mac' in kwargs:
            intf_args['ipv4_resolve_gateway'] = '0'
            intf_args['ipv4_manual_gateway_mac'] = kwargs['gateway_mac']

    if 'ipv6_addr' in kwargs:
        intf_args['ipv6_intf_addr'] = kwargs['ipv6_addr']
        intf_args['ipv6_intf_addr_step'] = kwargs.get('ipv6_addr_step', '00:00:00:01:00:00:00:00')
        if 'ipv6_gateway' in kwargs:
            intf_args['ipv6_gateway'] = kwargs['ipv6_gateway']
            intf_args['ipv6_gateway_step'] = kwargs.get('ipv6_addr_step', '00:00:00:01:00:00:00:00')
        if 'ipv6_prefix_length' in kwargs:
            intf_args['ipv6_prefix_length'] = kwargs.get('ipv6_prefix_length', '64')
        if 'mac_resolve' in kwargs:
            intf_args['ipv6_resolve_gateway'] = kwargs['mac_resolve']
        if 'gateway_mac' in kwargs:
            intf_args['ipv6_resolve_gateway'] = '0'
            intf_args['ipv6_manual_gateway_mac'] = kwargs['gateway_mac']
    if 'ip_addr' in kwargs or 'ipv6_addr' in kwargs:
        status_config = rt_handle.invoke('interface_config', **intf_args)
        if status_config['status'] != '1':
            result['status'] = '0'
            raise Exception("failed to add ip/ipv6 address for ethernet {}".format(ethernet_handle))
        else:
            if 'ipv4_handle' in status_config:
                ip_handle = status_config['ipv4_handle']
                rt_handle.link_ip_handle.append(ip_handle)
                rt_handle.handles[port]['ipv4_handle'] = ip_handle
                result['ipv4_handle'] = ip_handle
            if 'ipv6_handle' in status_config:
                ipv6_handle = status_config['ipv6_handle']
                rt_handle.link_ipv6_handle.append(ipv6_handle)
                rt_handle.handles[port]['ipv6_handle'].append(ipv6_handle)
                result['ipv6_handle'] = ipv6_handle

    if 'gre_dst_ip' in kwargs:
        ###config gre over ip
        gre_args = dict()
        gre_args['protocol_handle'] = ip_handle
        gre_args['gre_dst_ip_addr'] = kwargs['gre_dst_ip']
        if 'gre_dst_ip_step' in kwargs:
            gre_args['gre_dst_ip_addr_step'] = kwargs['gre_dst_ip_step']

        status_config = rt_handle.invoke('interface_config', **gre_args)
        if status_config['status'] != '1':
            result['status'] = '0'
            raise Exception("failed to add gre destination address for ip handle {}".format(ip_handle))
        else:
            gre_handle = status_config['greoipv4_handle']
            rt_handle.link_gre_handle.append(gre_handle)
            rt_handle.handles[port]['gre_handle'] = gre_handle
            result['gre_handle'] = gre_handle

    return result


def remove_link(rt_handle, **kwargs):
    # rt_handle.invoke('topology_config(mode='destroy', device_group_handle='/topology:2/deviceGroup:1')
    """
    :param rt_handle:                        RT object
    :param device_handle:               Device handle
    :return:
    """
    device_handle = kwargs['device_handle']
    status = rt_handle.invoke('topology_config', mode='destroy', device_group_handle=device_handle)
    rt_handle.device_group_handle.remove(device_handle)
    return status


def link_action(rt_handle, **kwargs):
    """
    start/stop links
    :param rt_handle:                    RT object
    :param kwargs:
    handle:                         device_group/ip handle
    action:                         start/stop/abort
    :return:
    status:                         1 or 0
    """
    control_args = dict()
    control_args['handle'] = kwargs['handle']
    if 'start' in kwargs['action']:
        control_args['action'] = 'start_protocol'
    if 'stop' in kwargs['action']:
        control_args['action'] = 'stop_protocol'
    if 'abort' in kwargs['action']:
        control_args['action'] = 'abort_protocol'
    return rt_handle.invoke('test_control', **control_args)


def add_traffic(rt_handle, **kwargs):
    #add_traffic(rt_handle, source=rt_handle.dhcpv4_client_handle, frame_size=[100,1500, 10])
    """
    set traffic streams
    :param rt_handle:                RT object
    :param kwargs:
    name:                       used defined stream name (optional)
    source:                     a list of traffic source handle
    destination:                a list of traffic destination handle
    multicast:                  a list of destination multicast address
    bidirectional:              1 or 0
    rate:                       traffic rate , can be mbps, pps, percent, for example: 1000mbps, 1000pps, 100%
    packets_count:              number of packets to be transmitted
    duration:                   number of seconds to transmit traffic
    type:                       traffic type "ipv4" or "ipv6" or "ethernet_vlan"
    mesh_type                   traffic mesh type, default is many_to_many, can be one_to_one
    dynamic_update:             dynamic_udate the address values from ppp
    frame_size:                 single value /a list [min max step]/[min max]
    track_by:                   how to track the statistics,  by default is
                                trafficItem and source destination value pair
    stream_id:                  needed when trying to modify existing traffic streams
    tcp_dst_port:               tcp destination port
    tcp_dst_port_mode:          tcp dst port mode(fixed, incr, decr)
    tcp_src_port:               tcp source port
    tcp_src_port_mode:          tcp src port mode(fixed, incr, decr)
    udp_dst_port:               udp destination port
    udp_src_port:               udp source port
    ip_precedence:              ip precedence value (0-7)
    ip_dscp:                    ip dscp value
    ipv6_traffic_class:         ipv6 traffic class value
    ipv6_traffic_class_mode:    ipv6 traffic class mode(fixed, incr,decr)
    egress_tracking:            egress tracking mode: dscp/ipv6TC/tos_precedence/outer_vlan_priority
    :return:
    status and hash
    """
    traffic_args = dict()
    traffic_args['traffic_generator'] = "ixnetwork_540"
    traffic_args['src_dest_mesh'] = kwargs.get('mesh_type', 'many_to_many')
    traffic_args['bidirectional'] = kwargs.get('bidirectional', '1')
    #import re
    if 'convert_to_raw' in kwargs:
        traffic_args['convert_to_raw'] = "1"
    if 'name' in kwargs:
        traffic_args['name'] = kwargs['name']
    if 'source' in kwargs:
        traffic_args['emulation_src_handle'] = kwargs['source']
    else:
        if 'type' in kwargs and 'v6' in kwargs['type']:
            traffic_args['emulation_src_handle'] = rt_handle.dhcpv6_client_handle
        else:
            traffic_args['emulation_src_handle'] = rt_handle.dhcpv4_client_handle + rt_handle.pppox_client_handle

    if 'destination' in kwargs:
        traffic_args['emulation_dst_handle'] = kwargs['destination']
    elif 'multicast' in kwargs:
        traffic_args['emulation_multicast_dst_handle'] = kwargs['multicast']
    else:
        if 'type' in kwargs and 'v6' in kwargs['type']:
            traffic_args['emulation_dst_handle'] = rt_handle.link_ipv6_handle
        else:
            traffic_args['emulation_dst_handle'] = rt_handle.link_ip_handle

    if 'rate' in kwargs:
        if 'mbps' in kwargs['rate']:
            traffic_args['rate_mbps'] = re.sub('mbps', '', kwargs['rate'])
        if 'pps' in kwargs['rate']:
            traffic_args['rate_pps'] = re.sub('pps', '', kwargs['rate'])
        if '%' in kwargs['rate']:
            traffic_args['rate_percent'] = re.sub('%', '', kwargs['rate'])
    else:
        traffic_args['rate_pps'] = '1000'

    if 'packets_count' in kwargs:
        traffic_args['transmit_mode'] = 'single_burst'
        #traffic_args['pkts_per_burst'] = kwargs['packets_count']
        traffic_args['number_of_packets_per_stream'] = kwargs['packets_count']

    if 'duration' in kwargs:
        traffic_args['duration'] = kwargs['duration']
    if 'stream_id' in kwargs:
        traffic_args['mode'] = 'modify'
    else:
        traffic_args['mode'] = 'create'

    if 'type' in kwargs:
        traffic_args['circuit_endpoint_type'] = kwargs['type']
    if 'dynamic_update' in kwargs:
        traffic_args['dynamic_update_fields'] = kwargs['dynamic_update']
    if 'ip_precedence' in kwargs:
        traffic_args['ip_precedence'] = kwargs['ip_precedence']
    if 'ip_precedence_mode' in kwargs:
        traffic_args['ip_precedence_mode'] = kwargs['ip_precedence_mode']
    if 'ip_precedence_step' in kwargs:
        traffic_args['ip_precedence_step'] = kwargs['ip_precedence_step']
    if 'ip_precedence_count' in kwargs:
        traffic_args['ip_precedence_count'] = kwargs['ip_precedence_count']
    if 'ip_dscp' in kwargs:
        traffic_args['ip_dscp'] = kwargs['ip_dscp']
    if 'ip_dscp_mode' in kwargs:
        traffic_args['ip_dscp_mode'] = kwargs['ip_dscp_mode']
    if 'ip_dscp_step' in kwargs:
        traffic_args['ip_dscp_step'] = kwargs['ip_dscp_step']
    if 'ip_dscp_count' in kwargs:
        traffic_args['ip_dscp_count'] = kwargs['ip_dscp_count']
    if 'frame_size' in kwargs:
        if isinstance(kwargs['frame_size'], list):
            if len(kwargs['frame_size']) == 3:
                traffic_args['length_mode'] = 'increment'
                traffic_args['frame_size_min'] = kwargs['frame_size'][0]
                traffic_args['frame_size_max'] = kwargs['frame_size'][1]
                traffic_args['frame_size_step'] = kwargs['frame_size'][2]
            if len(kwargs['frame_size']) == 2:
                traffic_args['length_mode'] = 'random'
                traffic_args['frame_size_min'] = kwargs['frame_size'][0]
                traffic_args['frame_size_max'] = kwargs['frame_size'][1]
    else:
        traffic_args['frame_size'] = kwargs.get('frame_size', '1000')
    ##can also track by sourceDestEndpointPair0
    traffic_args['track_by'] = kwargs.get('track_by', 'sourceDestValuePair0 trackingenabled0')
    if 'egress_tracking' in kwargs:
        traffic_args['egress_tracking'] = kwargs['egress_tracking']
    if 'ipv6_traffic_class' in kwargs:
        traffic_args['ipv6_traffic_class'] = kwargs['ipv6_traffic_class']
        traffic_args['ipv6_traffic_class_mode'] = kwargs.get('ipv6_traffic_class_mode', 'fixed')
        if 'ipv6_traffic_class_step' in kwargs:
            traffic_args['ipv6_traffic_class_step'] = kwargs['ipv6_traffic_class_step']
        if 'ipv6_traffic_class_count' in kwargs:
            traffic_args['ipv6_traffic_class_count'] = kwargs['ipv6_traffic_class_count']
    if 'traffic_generate' in kwargs:
        traffic_args['traffic_generate'] = kwargs['traffic_generate']
    if 'tcp_dst_port' in kwargs or 'tcp_src_port' in kwargs:
        traffic_args['l4_protocol'] = 'tcp'
        if 'tcp_dst_port' in kwargs:
            traffic_args['tcp_dst_port'] = kwargs['tcp_dst_port']
            traffic_args['tcp_dst_port_mode'] = kwargs.get('tcp_dst_port_mode', 'fixed')
            if 'tcp_dst_port_step' in kwargs:
                traffic_args['tcp_dst_port_step'] = kwargs['tcp_dst_port_step']
            if 'tcp_dst_port_count' in kwargs:
                traffic_args['tcp_dst_port_count'] = kwargs['tcp_dst_port_count']
        if 'tcp_src_port' in kwargs:
            traffic_args['tcp_src_port'] = kwargs['tcp_src_port']
            traffic_args['tcp_src_port_mode'] = kwargs.get('tcp_src_port_mode', 'fixed')
            if 'tcp_src_port_step' in kwargs:
                traffic_args['tcp_src_port_step'] = kwargs['tcp_src_port_step']
            if 'tcp_src_port_count' in kwargs:
                traffic_args['tcp_src_port_count'] = kwargs['tcp_src_port_count']

    if 'udp_dst_port' in kwargs or 'udp_src_port' in kwargs:
        traffic_args['l4_protocol'] = 'udp'
        if 'udp_dst_port' in kwargs:
            traffic_args['udp_dst_port'] = kwargs['udp_dst_port']
            traffic_args['udp_dst_port_mode'] = kwargs.get('udp_dst_port_mode', 'fixed')
            if 'udp_dst_port_step' in kwargs:
                traffic_args['udp_dst_port_step'] = kwargs['udp_dst_port_step']
            if 'udp_dst_port_count' in kwargs:
                traffic_args['udp_dst_port_count'] = kwargs['udp_dst_port_count']
        if 'udp_src_port' in kwargs:
            traffic_args['udp_src_port'] = kwargs['udp_src_port']
            traffic_args['udp_src_port_mode'] = kwargs.get('udp_src_port_mode', 'fixed')
            if 'udp_src_port_step' in kwargs:
                traffic_args['udp_src_port_step'] = kwargs['udp_src_port_step']
            if 'udp_src_port_count' in kwargs:
                traffic_args['udp_src_port_count'] = kwargs['udp_src_port_count']

    config_status = rt_handle.invoke('traffic_config', **traffic_args)
    if config_status['status'] == '1':
        if traffic_args['mode'] == 'create':
            rt_handle.traffic_item.append(config_status['stream_id'])
            rt_handle.stream_id.append(config_status['traffic_item'])
    return config_status


def set_traffic(rt_handle, **kwargs):
    """
    modify existing trafficitem
    :param rt_handle:                RT object
    :param kwargs:              see Ixia API library

    :return:
    status:                    1 or 0
    """
    result = dict()
    result['status'] = '1'
    if 'stream_id' not in kwargs:
        result['status'] = '0'
        print("stream_id is mandatory when modifying traffic item")
        return result
    result = rt_handle.invoke('traffic_config', mode='modify', **kwargs)
    return result


def get_traffic_stats(rt_handle, **kwargs):
    """

    :param kwargs:
    :param rt_handle:         RT object
    mode:                all/traffic_item
    :return:
    """
    stats_status = rt_handle.invoke('traffic_stats', **kwargs)
    return stats_status


def get_protocol_stats(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param mode:        'global_per_protocol'| 'global_per_port', by default is 'global_per_protocol'
    :return:
    """
    mode = kwargs.get('mode', 'global_per_protocol')
    stats_status = rt_handle.invoke('protocol_info', mode=mode)

    return stats_status


def traffic_action(rt_handle, **kwargs):
    """
    #traffic_action(rt_handle, action='start')
    :param rt_handle:            RT object
    :param kwargs:
    action:                 start/stop/delete/poll/regenerate/apply/clearstats/reset
    handle:                 specify a specific traffic item if needed
    :return:
    """
    traffic_args = dict()
    if 'timeout' in kwargs:
        traffic_args['max_wait_timer'] = kwargs['timeout']
    if 'handle' in kwargs:
        traffic_args['handle'] = kwargs['handle']
    traffic_args['action'] = 'run'
    if 'start' in kwargs['action']:
        traffic_args['action'] = 'sync_run'
    if 'stop' in kwargs['action']:
        traffic_args['action'] = 'stop'
    if 'poll' in kwargs['action']:
        traffic_args['action'] = 'poll'
    if 'clearstats' in kwargs['action']:
        traffic_args['action'] = 'clear_stats'
    if 'reset' in kwargs['action']:
        traffic_args['action'] = 'reset'
    if 'regenerate' in kwargs['action']:
        traffic_args['action'] = 'regenerate'
    if 'apply' in kwargs['action']:
        traffic_args['action'] = 'apply'
    if 'delete' in kwargs['action']:
        traffic_args['action'] = 'destroy'

    if 'delete' in kwargs['action'] and 'handle' in kwargs:
        traffic_args['mode'] = 'remove'
        traffic_args.pop('handle')
        traffic_args.pop('action')
        traffic_args['stream_id'] = kwargs['handle']
        result = rt_handle.invoke('traffic_config', **traffic_args)
        print(traffic_args)
    else:
        result = rt_handle.invoke('traffic_control', **traffic_args)
    if result['status'] == '1' and 'delete' in kwargs['action']:
        if 'handle' in kwargs:
            rt_handle.traffic_item.remove(kwargs['handle'])
        else:
            rt_handle.traffic_item.clear()
    return result


def start_all(rt_handle):
    """
    start all protocols
    :param rt_handle:                    RT object
    :return:
    a dictionary of status and other information
    """
    return rt_handle.invoke('test_control', action='start_all_protocols')


def stop_all(rt_handle):
    """
    stop all protocols
    :param rt_handle:                    RT object
    :return:
    a dictionary of status and other information
    """
    return rt_handle.invoke('test_control', action='stop_all_protocols')


def add_igmp_client(rt_handle, **kwargs):
    """
    :param rt_handle:                    RT object
    :param kwargs:
    handle:                         dhcp client handle or pppoe client handle
    version:                        version 2 or 3, default is 2
    filter_mode:                    include/exclude, default is include
    iptv:                           1 or 0, default is 0
    group_range:                    multicast group range, default is 1
    group_range_step:               the pattern that the range start address, default is 0.0.1.0
    group_start_addr:               multicast group start address
    group_step:                     group step pattern
    group_count:                    group counts
    src_grp_range:                  multicast source group range, default is 1
    src_grp_range_step:             multicast source group range step pattern, default is 0.0.1.0
    src_grp_start_addr:             multicast source group start address
    src_grp_step:                   multicast soutce group step pattern, default is 0.0.0.1
    src_grp_count:                  multicast source group count

    :return:
    result                          a dictionary include status, igmp_host_handle, igmp_group_handle, igmp_source_handle
    """
    result = dict()
    result['status'] = '1'
    igmp_param = dict()
    igmp_param['handle'] = kwargs.get('handle')
    igmp_param['mode'] = kwargs.get('mode', 'create')
    igmp_param['active'] = kwargs.get('active', '1')
    igmp_param['filter_mode'] = kwargs.get('filter_mode', 'include')
    igmp_param['enable_iptv'] = kwargs.get('iptv', '0')
    igmp_param['igmp_version'] = 'v' + str(kwargs.get('version', '2'))

    _result_ = rt_handle.invoke('emulation_igmp_config', **igmp_param)
    print(_result_)
    if _result_['status'] != '1':
        result['log'] = "failed to add igmp client"
        result['status'] = '0'
        return result
    else:
        igmp_handle = _result_['igmp_host_handle']
        rt_handle.igmp_handles[kwargs.get('handle')] = igmp_handle
        rt_handle.igmp_handle_to_group[igmp_handle] = {}
        result['igmp_host_handle'] = igmp_handle

    addr_param = dict()
    addr_param['start'] = kwargs.get('group_start_addr', '225.0.0.1')
    addr_param['step'] = kwargs.get('group_range_step', "0.0.1.0")
    addr_param['count'] = kwargs.get('group_range', "1")
    multivalue_2_handle = __add_addr_custom_pattern(rt_handle, **addr_param)
    mcast_args = dict()
    mcast_args['mode'] = "create"
    mcast_args['ip_addr_start'] = multivalue_2_handle
    mcast_args['ip_addr_step'] = kwargs.get('group_step', "0.0.0.1")
    mcast_args['num_groups'] = kwargs.get('group_count', "1")
    mcast_args['active'] = kwargs.get('active', "1")
    _result_ = rt_handle.invoke('emulation_multicast_group_config', **mcast_args)
    print(_result_)
    if _result_['status'] != '1':
        result['status'] = '0'
    else:
        igmp_group_handle = _result_['multicast_group_handle']
        rt_handle.igmp_handle_to_group[igmp_handle]['group_handle'] = igmp_group_handle


    src_addr_param = dict()
    src_addr_param['start'] = kwargs.get('src_grp_start_addr', "10.10.10.1")
    src_addr_param['step'] = kwargs.get('src_grp_range_step', "0.0.1.0")
    src_addr_param['count'] = kwargs.get('src_grp_range', "1")
    multivalue_3_handle = __add_addr_custom_pattern(rt_handle, **src_addr_param)
    src_args = dict()
    src_args['mode'] = "create"
    src_args['ip_addr_start'] = multivalue_3_handle
    src_args['ip_addr_step'] = kwargs.get('src_grp_step', "0.0.0.1")
    src_args['num_sources'] = kwargs.get('src_grp_count', "1")
    src_args['active'] = "1"
    _result_ = rt_handle.invoke('emulation_multicast_source_config', **src_args)
    print(_result_)
    if _result_['status'] != '1':
        result['status'] = '0'
    else:
        igmp_source_handle = _result_['multicast_source_handle']
        rt_handle.igmp_handle_to_group[igmp_handle]['src_group_handle'] = igmp_source_handle
    grp_args = dict()
    grp_args['mode'] = kwargs.get('mode', "create")
    grp_args['g_filter_mode'] = kwargs.get('filter_mode', "include")
    grp_args['group_pool_handle'] = igmp_group_handle
    grp_args['no_of_grp_ranges'] = kwargs.get('group_range', "1")
    grp_args['no_of_src_ranges'] = kwargs.get('src_grp_range', "1")
    grp_args['session_handle'] = igmp_handle
    grp_args['source_pool_handle'] = igmp_source_handle
    _result_ = rt_handle.invoke('emulation_igmp_group_config', **grp_args)
    print(_result_)
    if _result_['status'] != '1':
        result['status'] = '0'
        result['log'] = "failed to config the igmp group and src group set"
    else:
        result['igmp_group_handle'] = _result_['igmp_group_handle']
        result['igmp_source_handle'] = _result_['igmp_source_handle']
        rt_handle.igmp_handle_to_group[igmp_handle]['igmp_group_handle'] = _result_['igmp_group_handle']
        rt_handle.igmp_handle_to_group[igmp_handle]['igmp_source_handle'] = _result_['igmp_source_handle']

    return result


def igmp_client_action(rt_handle, **kwargs):
    """
    :param rt_handle:                                RT object
    :param kwargs:
    handle:                                     igmp host handle
    action:                                     start/stop/join/leave/igmp_send_specific_query
    start_group_addr:                           only used for igmp_send_secific_query
    group_count:                                only used for igmp_send_secific_query
    start_source_addr:                          only used for igmp_send_secific_query
    source_count:                               only used for igmp_send_secific_query
    :return:
    """
    action_param = dict()
    action_param['handle'] = kwargs['handle']
    if 'action' in kwargs:
        action_param['mode'] = kwargs['action']
    if 'start_group_addr' in kwargs:
        action_param['start_group_address'] = kwargs['start_group_addr']
    if 'group_count' in kwargs:
        action_param['group_count'] = kwargs['group_count']
    if 'start_source_addr' in kwargs:
        action_param['start_source_address'] = kwargs['start_source_addr']
    if 'source_count' in kwargs:
        action_param['source_count'] = kwargs['source_count']
    return rt_handle.invoke('emulation_igmp_control', **action_param)


def __add_addr_custom_pattern(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    :return:        multivalue_handle
    """

    if 'type' not in kwargs:
        ip_type = 'v4'
    else:
        ip_type = kwargs['type']

    _result_ = rt_handle.invoke('multivalue_config', pattern="custom")
    print(_result_)
    multivalue_handle = _result_['multivalue_handle']
    if 'v4' in ip_type:
        custom_step = "0.0.0.0"
        custom_start = kwargs.get('start')
        increment_value = kwargs.get('step')

    else:
        custom_step = ipaddress.IPv6Address('::').exploded
        custom_start = ipaddress.IPv6Address(kwargs['start']).exploded
        increment_value = ipaddress.IPv6Address(kwargs['step']).exploded
    _result_ = rt_handle.invoke('multivalue_config',
                                multivalue_handle=multivalue_handle,
                                custom_start=custom_start,
                                custom_step=custom_step,)
    print(_result_)
    custom_1_handle = _result_['custom_handle']

    _result_ = rt_handle.invoke('multivalue_config',
                                custom_handle=custom_1_handle,
                                custom_increment_value=increment_value,
                                custom_increment_count=kwargs.get('count'),)
    print(_result_)
    return multivalue_handle


def add_mld_client(rt_handle, **kwargs):
    """
    :param rt_handle:                     RT object
    :param kwargs:
     handle:                         dhcpv6 client handle
     version:                        version 1 or 2, default is 1
     filter_mode:                    include/exclude, default is include
     iptv:                           1 or 0, default is 0
     group_range:                    multicast group range, default is 1
     group_range_step:               the pattern that the range start address, default is ::1:0
     group_start_addr:               multicast group start address
     group_step:                     group step pattern, default is ::1
     group_count:                    group counts
     src_grp_range:                  multicast source group range, default is 1
     src_grp_range_step:             multicast source group range step pattern, default is ::1:0
     src_grp_start_addr:             multicast source group start address
     src_grp_step:                   multicast soutce group step pattern, default is ::1
     src_grp_count:                  multicast source group count

     :return:
     result                          a dictionary include status, mld_host_handle, mld_group_handle, mld_source_handle
     """
    result = dict()
    result['status'] = '1'
    mld_param = dict()
    mld_param['handle'] = kwargs.get('handle')
    mld_param['mode'] = kwargs.get('mode', 'create')
    mld_param['active'] = kwargs.get('active', '1')
    mld_param['filter_mode'] = kwargs.get('filter_mode', 'include')
    mld_param['enable_iptv'] = kwargs.get('iptv', '0')
    mld_param['mld_version'] = 'v' + str(kwargs.get('version', '1'))

    _result_ = rt_handle.invoke('emulation_mld_config', **mld_param)
    print(_result_)
    if _result_['status'] != '1':
        result['log'] = "failed to add mld client"
        result['status'] = '0'
        return result
    else:
        mld_handle = _result_['mld_host_handle']
        rt_handle.mld_handles[kwargs.get('handle')] = mld_handle
        rt_handle.mld_handle_to_group[mld_handle] = {}
        result['mld_host_handle'] = mld_handle

    addr_param = dict()
    addr_param['start'] = ipaddress.IPv6Address(kwargs.get('group_start_addr', 'ff03::1')).exploded
    addr_param['step'] = ipaddress.IPv6Address(kwargs.get('group_range_step', "0:0:0:0:0:0:1:0")).exploded
    addr_param['count'] = kwargs.get('group_range', "1")
    addr_param['type'] = 'v6'
    multivalue_2_handle = __add_addr_custom_pattern(rt_handle, **addr_param)
    mcast_args = dict()
    mcast_args['mode'] = "create"
    mcast_args['ip_addr_start'] = multivalue_2_handle
    mcast_args['ip_addr_step'] = ipaddress.IPv6Address(kwargs.get('group_step', "0:0:0:0:0:0:0:1")).exploded
    mcast_args['num_groups'] = kwargs.get('group_count', "1")
    mcast_args['active'] = kwargs.get('active', "1")
    _result_ = rt_handle.invoke('emulation_multicast_group_config', **mcast_args)
    print(_result_)
    if _result_['status'] != '1':
        result['status'] = '0'
    else:
        mld_group_handle = _result_['multicast_group_handle']
        rt_handle.mld_handle_to_group[mld_handle]['group_handle'] = mld_group_handle

    src_addr_param = dict()
    src_addr_param['start'] = ipaddress.IPv6Address(kwargs.get('src_grp_start_addr', "200::1")).exploded
    src_addr_param['step'] = ipaddress.IPv6Address(kwargs.get('src_grp_range_step', "0:0:0:0:0:0:1:0")).exploded
    src_addr_param['count'] = kwargs.get('src_grp_range', "1")
    src_addr_param['type'] = 'v6'
    multivalue_3_handle = __add_addr_custom_pattern(rt_handle, **src_addr_param)
    src_args = dict()
    src_args['mode'] = "create"
    src_args['ip_addr_start'] = multivalue_3_handle
    src_args['ip_addr_step'] = ipaddress.IPv6Address(kwargs.get('src_grp_step', "0:0:0:0:0:0:0:1")).exploded
    src_args['num_sources'] = kwargs.get('src_grp_count', "1")
    src_args['active'] = "1"
    _result_ = rt_handle.invoke('emulation_multicast_source_config', **src_args)
    print(_result_)
    if _result_['status'] != '1':
        result['status'] = '0'
    else:
        mld_source_handle = _result_['multicast_source_handle']
        rt_handle.mld_handle_to_group[mld_handle]['src_group_handle'] = mld_source_handle
    grp_args = dict()
    grp_args['mode'] = kwargs.get('mode', "create")
    grp_args['g_filter_mode'] = kwargs.get('filter_mode', "include")
    grp_args['group_pool_handle'] = mld_group_handle
    grp_args['no_of_grp_ranges'] = kwargs.get('group_range', "1")
    grp_args['no_of_src_ranges'] = kwargs.get('src_grp_range', "1")
    grp_args['session_handle'] = mld_handle
    grp_args['source_pool_handle'] = mld_source_handle
    _result_ = rt_handle.invoke('emulation_mld_group_config', **grp_args)
    print(_result_)
    if _result_['status'] != '1':
        result['status'] = '0'
        result['log'] = "failed to config the mld group and src group set"
    else:
        result['mld_group_handle'] = _result_['mld_group_handle']
        result['mld_source_handle'] = _result_['mld_source_handle']
        rt_handle.mld_handle_to_group[mld_handle]['mld_group_handle'] = _result_['mld_group_handle']
        rt_handle.mld_handle_to_group[mld_handle]['mld_source_handle'] = _result_['mld_source_handle']

    return result


def mld_client_action(rt_handle, **kwargs):
    """
    :param rt_handle:                                RT object
    :param kwargs:
    handle:                                     mld host handle
    action:                                     start/stop/join/leave/mld_send_specific_query
    start_group_addr:                           only used for mld_send_secific_query
    group_count:                                only used for mld_send_secific_query
    start_source_addr:                          only used for mld_send_secific_query
    source_count:                               only used for mld_send_secific_query
    :return:
    """
    action_param = dict()
    action_param['handle'] = kwargs['handle']
    if 'action' in kwargs:
        action_param['mode'] = kwargs['action']
    if 'start_group_addr' in kwargs:
        action_param['start_group_address'] = kwargs['start_group_addr']
    if 'group_count' in kwargs:
        action_param['group_count'] = kwargs['group_count']
    if 'start_source_addr' in kwargs:
        action_param['start_source_address'] = kwargs['start_source_addr']
    if 'source_count' in kwargs:
        action_param['source_count'] = kwargs['source_count']
    return rt_handle.invoke('emulation_mld_control', **action_param)


def add_application_traffic(rt_handle, **kwargs):
    """

    :param rt_handle:
    :param kwargs:
     type:                             ipv4/ipv6
     source:                           source handle
     destination:                      destination handle
     flow:                             application, e,g, "HTTP_302_Redirect"
     name:                             stream name
     per_ip_stats:                     1 or 0, default is 0

    :return:
    """
    app_args = dict()
    app_args['mode'] = kwargs.get('mode', 'create')
    app_args['objective_type'] = kwargs.get('objective_type', "users")
    app_args['objective_value'] = kwargs.get('objective_value', "100")
    app_args['objective_distribution'] = kwargs.get('objective_distribution', "apply_full_objective_to_each_port")
    app_args['enable_per_ip_stats'] = kwargs.get('per_ip_stats', "0")
    if 'name' in kwargs:
        app_args['name'] = kwargs['name']
    if 'source' in kwargs:
        app_args['emulation_src_handle'] = kwargs['source']
    if 'destination' in kwargs:
        app_args['emulation_dst_handle'] = kwargs['destination']

    if 'flows' in kwargs:
        app_args['flows'] = kwargs['flows']

    if 'type' in kwargs:
        if 'v4' in kwargs['type']:
            app_args['circuit_endpoint_type'] = "ipv4_application_traffic"
        if 'v6' in kwargs['type']:
            app_args['circuit_endpoint_type'] = "ipv6_application_traffic"
    result = rt_handle.invoke('traffic_l47_config', **app_args)
    return result


def add_bgp(rt_handle, **kwargs):
    """

    :param rt_handle:
    :param kwargs:
    handle:                     ipv4 handle or ipv6 handle
    type:                       external/internal
    remote_ip:                  neighbor ip address
    local_as:                   Local as number
    hold_time:                  bgp hold time
    restart_time:               bgp restart time
    keepalive:                  bgp keepalive timer
    router_id:                  bgp router id
    stale_time:                 bgp stale time
    enable_flap:                bgp flap enable 1/0
    flap_down_time:             flap down time
    flap_up_time:               flap up time
    graceful_restart:           graceful restart 1/0
    prefix_group:               list of prefixes which was a dictionary include
                                network_prefix:             bgp network prefix
                                network_step:               bgp network prefix increment step
                                network_count:              bgp network counter
                                sub_prefix_length:      network sub prefix length
                                sub_prefix_count:       network sub prefix count

    :return:                    result dictionary: status, bgp_handle, network_group_handle,
    """
    result = dict()
    result['status'] = '1'
    result['network_group_handle'] = []
    bgp_params = dict()
    bgp_params['mode'] = 'enable'
    bgp_params['handle'] = kwargs['handle']
    match = re.match(r'\/topology:\d+\/deviceGroup:\d+', kwargs['handle'])
    devicegroup_handle = match.group(0)
    bgp_params['neighbor_type'] = kwargs['type']
    bgp_params['local_as'] = kwargs['local_as']
    if 'hold_time' in kwargs:
        bgp_params['hold_time'] = kwargs['hold_time']
    if 'restart_time' in kwargs:
        bgp_params['restart_time'] = kwargs['restart_time']
    if 'keepalive' in kwargs:
        bgp_params['keepalive_timer'] = kwargs['keepalive']
    if 'enable_flap' in kwargs:
        bgp_params['enable_flap'] = kwargs['enable_flap']
        bgp_params['flap_up_time'] = kwargs['flap_up_time']
        bgp_params['flap_down_time'] = kwargs['flap_down_time']
    if 'stale_time' in kwargs:
        bgp_params['stale_time'] = kwargs['stale_time']
    if 'graceful_restart' in kwargs:
        bgp_params['graceful_restart_enable'] = kwargs['graceful_restart']

    if 'v4' in kwargs['handle']:
        bgp_params['ip_version'] = '4'
        bgp_params['remote_ip_addr'] = kwargs['remote_ip']
        if 'router_id' in kwargs:
            bgp_params['router_id'] = kwargs['router_id']
    if 'v6' in kwargs['handle']:
        bgp_params['ip_version'] = '6'
        bgp_params['remote_ipv6_addr'] = kwargs['remote_ip']

    _result_ = rt_handle.invoke('emulation_bgp_config', **bgp_params)
    if _result_['status'] == '1':
        bgp_handle = _result_['bgp_handle']
        rt_handle.bgp_handle.append(bgp_handle)
        result['bgp_handle'] = bgp_handle
    else:
        return _result_
    for prefix in kwargs['prefix_group']:

        _result_ = rt_handle.invoke('multivalue_config',
                                    pattern="counter",
                                    counter_start=prefix['network_prefix'],
                                    counter_step=prefix['network_step'],
                                    counter_direction="increment",)
        multivalue_11_handle = _result_['multivalue_handle']

        network_params = dict()
        network_params['protocol_handle'] = devicegroup_handle
        if 'network_count' in kwargs:
            network_params['multiplier'] = prefix['network_count']
        network_params['connected_to_handle'] = bgp_handle
        if 'v4' in kwargs['handle']:
            network_params['type'] = "ipv4-prefix"
            network_params['ipv4_prefix_network_address'] = multivalue_11_handle
            network_params['ipv4_prefix_length'] = prefix['sub_prefix_length']
            if 'sub_prefix_count' in prefix:
                network_params['ipv4_prefix_number_of_addresses'] = prefix['sub_prefix_count']
        if 'v6' in kwargs['handle']:
            network_params['type'] = "ipv6-prefix"
            network_params['ipv6_prefix_network_address'] = multivalue_11_handle
            network_params['ipv6_prefix_length'] = prefix['sub_prefix_length']
            if 'sub_prefix_count' in prefix:
                network_params['ipv6_prefix_number_of_addresses'] = prefix['sub_prefix_count']

        _result_ = rt_handle.invoke('network_group_config', **network_params)
        if _result_['status'] == '1':
            network_group_handle = _result_['network_group_handle']
            result['network_group_handle'].append(network_group_handle)
        else:
            return _result_
        route_params = dict()
        if 'ipv4_prefix_pools_handle' in _result_:
            prefix_pool_handle = _result_['ipv4_prefix_pools_handle']
            route_params['ip_version'] = '4'
            route_params["ipv4_unicast_nlri"] = "1"
        if 'ipv6_prefix_pools_handle' in _result_:
            prefix_pool_handle = _result_['ipv6_prefix_pools_handle']
            route_params['ip_version'] = '6'
            route_params["ipv6_unicast_nlri"] = "1"
        route_params['handle'] = network_group_handle
        route_params['mode'] = 'create'
        route_params['prefix'] = multivalue_11_handle
        route_params['num_routes'] = prefix.get('sub_prefix_count', '1')
        route_params['prefix_from'] = prefix['sub_prefix_length']
        route_params['max_route_ranges'] = prefix.get('network_count', '1')

        _result_ = rt_handle.invoke('emulation_bgp_route_config', **route_params)
        if _result_['status'] != '1':
            result['status'] = '0'
    return result


def bgp_action(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    handle:                                 bgp session handle
    action:                                 start/stop/restart/abort/restart_down/delete
    :return:
    """
    bgp_params = dict()
    bgp_params['mode'] = kwargs['action']
    bgp_params['handle'] = kwargs['handle']
    if 'delete' in kwargs['action']:
        match = re.match(r'\/topology:\d+\/deviceGroup:\d+', kwargs['handle'])
        devicegroup_handle = match.group(0)
        rt_handle.invoke('test_control', handle=devicegroup_handle, action='stop_protocol')
        return rt_handle.invoke('emulation_bgp_config', handle=kwargs['handle'], mode='delete')

    return rt_handle.invoke('emulation_bgp_control', **kwargs)


def add_l2tp_server(rt_handle, **kwargs):
    """
    rt_handle.invoke('l2tp_config(mode='lns',port_handle='1/1/3',lns_host_name='ixia_lns',
    tun_auth='authenticate_hostname', secret='ixia', hostname='lac', l2_encap='ethernet_ii_vlan',
    l2tp_dst_addr='10.2.0.2', l2tp_src_addr='10.2.0.1', num_tunnels=1,auth_mode='pap_or_chap',
    username='test', password='pwd', l2tp_src_prefix_len=24)
    :param rt_handle:                                            RT object
    :param kwargs:
    port:                                port
    handle:                             ipv4 handle
    tun_auth_enable:                    1 or 0, authentication method for tunnel('authenticate_hostname'/
                                        tunnel_authentication_disabled), default is 1
    tun_secret:                         tunnel secret
    tun_hello_req:                      send tunnel hello request , value could be 1/0
    l2tp_dst_addr:                      l2tp destionation start address(mandatory)
    l2tp_src_addr:                      l2tp source start address(mandatory)
    netmask:                            ip address netmask
    hostname:                           lac hostname, default is 'lac'
    lns_host_name:                      lns hostname, default is 'ixia_lns'
    tun_hello_req:                      send tunnel hello request , value could be 1/0
    l2tp_src_prefix_len:                l2tp source prefix length, default is 24
    auth_mode:                          authentication mode, none/pap/chap/pap_or_chap, default is none
    username:                           username for authentication
    password:                           password for authentication
    ip_cp:                              ip_cp mode, could be ipv4_cp/ipv6_cp/dual_stack, default is ipv4_cp
    vlan_id:                            interface vlan id
    lease_time                             pool address lease time
    dhcpv6_ia_type:                        v6 IA type "iana, iapd"
    pool_prefix_start:                     v6 PD start prefix
    pool_prefix_length:                    v6 prefix length
    pool_prefix_size:                      v6 prefix pool size
    :return:
    """

    lns_params = dict()
    link_args = dict()
    lns_params['mode'] = 'lns'
    lns_params['num_tunnels'] = '1'
    lns_params['hostname'] = kwargs.get('hostname', 'mx_lac')
    lns_params['lns_host_name'] = kwargs.get('lns_host_name', 'ixia_lns')
    #lns_params['vlan_id'] = kwargs.get('vlan_id', '1')
    #lns_params['l2_encap'] = 'ethernet_ii_vlan'
    if 'handle' in kwargs:
        lns_params['handle'] = kwargs['handle']
    if 'port' in kwargs:
        port = kwargs['port']
        link_args['port'] = port
        link_args['ip_addr'] = kwargs['l2tp_dst_addr']
        link_args['gateway'] = kwargs['l2tp_src_addr']
        link_args['netmask'] = kwargs['netmask']
        if 'vlan_id' in kwargs:
            link_args['vlan_start'] = kwargs['vlan_id']
        _result_ = add_link(rt_handle, **link_args)
        if _result_['status'] == '0':
            raise Exception("failed to add ip address for lns")
        ipv4_handle = _result_['ipv4_handle']
        ethernet_handle = _result_['ethernet_handle']
        lns_params['handle'] = ipv4_handle

    # if port not in rt_handle.handles:
    #     _result_ = add_topology(rt_handle, port_list=[port])
    # if rt_handle.ae and port in rt_handle.ae:
    #     device_handle = rt_handle.handles[port]['device_group_handle'][0]
    #     lns_params['handle'] = device_handle
    # else:
    #     lns_params['port_handle'] = rt_handle.port_to_handle_map[kwargs['port']]
    # lns_params['l2tp_src_addr'] = kwargs['l2tp_src_addr']
    # lns_params['l2tp_dst_addr'] = kwargs['l2tp_dst_addr']
    if int(kwargs.get('tun_auth_enable', '1')):
        lns_params['tun_auth'] = 'authenticate_hostname'
    else:
        lns_params['tun_auth'] = 'tunnel_authentication_disabled'
    lns_params['secret'] = kwargs.get('tun_secret', 'secret')
    #lns_params['l2tp_src_prefix_len'] = kwargs.get('l2tp_src_prefix_len', '24')
    lns_params['sessions_per_tunnel'] = kwargs.get('total_sessions', '32000')
    if 'tun_hello_req' in kwargs:
        lns_params['hello_req'] = kwargs['tun_hello_req']
    if 'tun_hello_interval' in kwargs:
        lns_params['hello_interval'] = kwargs['tun_hello_interval']
    if 'username' in kwargs:
        lns_params['username'] = kwargs['username']
    if 'password' in kwargs:
        lns_params['password'] = kwargs['password']
    # if 'ip_cp' in kwargs:
    #     lns_params['ip_cp'] = kwargs['ip_cp']
    #     if 'dual' in kwargs['ip_cp'] or 'v6' in kwargs['ip_cp']:
    #         lns_params['dhcpv6_hosts_enable'] = '1'
    lns_params['auth_mode'] = kwargs.get('auth_mode', 'pap_or_chap')
    result = rt_handle.invoke('l2tp_config', **lns_params)
    if result['status'] == "1":
        rt_handle.lns_handle.append(result['lns_handle'])
        rt_handle.l2tp_server_session_handle.append(result['pppox_server_sessions_handle'])
        pppox_server_handle = result['pppox_server_handle']
        if 'dhcpv6' in result:
            rt_handle.dhcpv6_server_handle.append(result['dhcpv6_server_handle'])
    ##add dhcpv6 server if necessary
    if 'ip_cp' in kwargs:
        if 'dual' in kwargs['ip_cp'] or 'v6' in kwargs['ip_cp']:
            kwargs['handle'] = pppox_server_handle
            config = add_dhcp_server(rt_handle, **kwargs)
            if config['status'] == '0':
                result['status'] = '0'
            result['dhcpv6_server_handle'] = config['dhcpv6_server_handle']
    return result


def l2tp_server_action(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    handle:                     lns handle
    action:                     start/stop
    :return:
    """
    param = dict()
    match = re.match(r'\/topology:\d+\/deviceGroup:\d+', kwargs['handle'])
    param['handle'] = match.group(0)
    param['action'] = kwargs['action']
    return rt_handle.invoke('test_control', **param)


def add_l2tp_client(rt_handle, **kwargs):
    """
    maximum sessions per port is 32000
    :param rt_handle:                        RT Object from toby
    :param kwargs:
    port                                tester port(mandatory)
    num_tunnels_per_lac:                number of tunnels configured per LAC
    sessions_per_tunnel:                sessions per LAC, default is 5
    tun_auth_enable:                    1 or 0,       authentication method for tunnel('authenticate_hostname'/
                                        tunnel_authentication_disabled), default is 1
    tun_secret:                         tunnel secret
    tun_hello_req:                      send tunnel hello request , value could be 1/0
    tun_hello_interval:                 tunnel hello interval, valid only when tun_hello_req is 1
    echo_req:                           enable/disable ppp keepalive request by RT
    echo_req_interval:                  ppp keepalive request interval
    l2tp_dst_addr:                      l2tp destionation start address, default is '100.0.0.1'
    l2tp_src_addr:                      l2tp source start address(mandatory)
    hostname:                           lac hostname, default is 'lac'
    l2tp_src_count:                     l2tp source address counts(same as lac count), default is 1
    l2tp_src_step:                      l2tp source address step, default is 0.0.1.0
    l2tp_src_gw:                        l2tp source gateway(mandatory)
    l2tp_src_gw_step:                   l2tp source gateway step, default is 0.0.1.0
    l2tp_src_prefix_len:                l2tp source prefix length, default is 24
    l2tp_dst_step:                      l2tp destintion address step
    vlan_id:                            start vlan id for the connection, default is 1
    vlan_id_step:                       vlan id step, default is 1
    auth_mode:                          authentication mode, none/pap/chap/pap_or_chap, default is none
    username:                           username for authentication
    password:                           password for authentication
    ip_cp:                              ip_cp mode, could be ipv4_cp/ipv6_cp/dual_stack, default is ipv4_cp
    dhcpv6_ia_type:                     can be iana/iapd/iana_iapd, default is iapd
    #domain_name:                        domain name, (eg. if you set this to abc?.com, the domain name will increase
                                        from 1, and repeat sessions_per_tunnel)
    :return:                            dictionary of status , ethernet_handle, ipv4_handle, lac_handle,
                                        pppox_client_handle, dhcpv6_client_handle
    """
    lac_params = dict()
    lac_params['mode'] = 'lac'
    # lac_params['vlan_id'] = kwargs.get('vlan_id', '1')
    # lac_params['vlan_id_step'] = kwargs.get('vlan_id_step', '1')
    #lac_params['l2_encap'] = 'ethernet_ii_vlan'
    #lac_params['port_handle'] = rt_handle.port_to_handle_map[kwargs['port']]
    # port = kwargs['port']
    # if port not in rt_handle.handles:
    #     _result_ = add_topology(rt_handle, port_list=[port])
    if 'port' in kwargs:
        port = kwargs['port']
        link_args = dict()
        if port not in rt_handle.handles:
            _result_ = add_topology(rt_handle, port_list=[port])
        link_args['port'] = port
        link_args['ip_addr'] = kwargs['l2tp_src_addr']
        link_args['ip_addr_step'] = kwargs.get('l2tp_src_step', '0.0.1.0')
        link_args['gateway'] = kwargs['l2tp_src_gw']
        v4addr = ipaddress.IPv4Interface('0.0.0.0/' + str(kwargs.get('l2tp_src_prefix_len', '24')))
        link_args['netmask'] = str(v4addr.netmask)
        link_args['count'] = kwargs.get('l2tp_src_count', '1')
        if 'vlan_id' in kwargs:
            link_args['vlan_start'] = kwargs['vlan_id']
        if 'vlan_id_step' in kwargs:
            link_args['vlan_step'] = kwargs['vlan_id_step']
        _result_ = add_link(rt_handle, **link_args)
        if _result_['status'] == '0':
            raise Exception("failed to add ip address for lns")
        ipv4_handle = _result_['ipv4_handle']
        ethernet_handle = _result_['ethernet_handle']
        lac_params['handle'] = ipv4_handle
    # if rt_handle.ae and port in rt_handle.ae:
    #     device_handle = rt_handle.handles[port]['device_group_handle'][0]
    #     lac_params['handle'] = device_handle
    # else:
    #     lac_params['port_handle'] = rt_handle.port_to_handle_map[kwargs['port']]
    if int(kwargs.get('tun_auth_enable', '1')):
        lac_params['tun_auth'] = 'authenticate_hostname'
    else:
        lac_params['tun_auth'] = 'tunnel_authentication_disabled'
    if 'tun_secret' in kwargs:
        lac_params['secret'] = kwargs['tun_secret']
    lac_params['num_tunnels'] = kwargs.get('num_tunnels_per_lac', '1')
    lac_params['sessions_per_tunnel'] = kwargs.get('sessions_per_tunnel', '5')
    lac_params['l2tp_dst_addr'] = kwargs.get('l2tp_dst_addr', '100.0.0.1')
    if 'l2tp_dst_step' in kwargs:
        lac_params['l2tp_dst_step'] = kwargs['l2tp_dst_step']
    if 'l2tp_dst_count' in kwargs:
        lac_params['l2tp_dst_count'] = kwargs
#    lac_params['l2tp_src_addr'] = kwargs['l2tp_src_addr']
#   lac_params['l2tp_src_gw'] = kwargs['l2tp_src_gw']
#    lac_params['l2tp_src_count'] = kwargs.get('l2tp_src_count', '1')
#    lac_params['l2tp_src_step'] = kwargs.get('l2tp_src_step', '0.0.1.0')
#    lac_params['l2tp_src_gw_step'] = kwargs.get('l2tp_src_gw_step', lac_params['l2tp_src_step'])
#    lac_params['l2tp_src_prefix_len'] = kwargs.get('l2tp_src_prefix_len', '24')
    lac_params['hostname'] = kwargs.get('hostname', 'lac')
    if 'echo_req' in kwargs:
        lac_params['echo_req'] = kwargs['echo_req']
        if 'echo_req_interval' in kwargs:
            lac_params['echo_req_interval'] = kwargs['echo_req_interval']
    if 'mode' in kwargs:
        lac_params['action'] = kwargs['mode']
    #lac_params['tun_distribution'] = kwargs.get('tun_distribution', 'next_tunnelfill_tunnel')
    if 'tun_hello_req' in kwargs:
        lac_params['hello_req'] = kwargs['tun_hello_req']
        if 'tun_hello_interval' in kwargs:
            lac_params['hello_interval'] = kwargs['tun_hello_interval']
    if 'l2tp_dst_step' in kwargs:
        lac_params['l2tp_dst_step'] = kwargs['l2tp_dst_step']
    # if 'ip_cp' in kwargs:
    #     lac_params['ip_cp'] = kwargs['ip_cp']
    #     if 'ipv6' in kwargs['ip_cp'] or 'dual' in kwargs['ip_cp']:
    #         lac_params['dhcpv6_hosts_enable'] = '1'
    #         lac_params['dhcp6_pd_client_range_ia_type'] = kwargs.get('dhcpv6_ia_type', 'iapd')

    lac_params['auth_mode'] = kwargs.get('auth_mode', 'none')

    if 'username' in kwargs:
        lac_params['username'] = kwargs['username']
        if '?' in lac_params['username']:
            increment = '{Inc:' + '1,,,' + str(lac_params['sessions_per_tunnel']) +'}'
            lac_params['username'] = kwargs['username'].replace('?', increment)
    if 'password' in kwargs:
        lac_params['password'] = kwargs['password']

    if int(kwargs.get('l2tp_src_count', '1')) > 1:
        ##increase the lac hostname
        lac_params['hostname'] = lac_params['hostname'] + '{Inc:1,,,' + str(lac_params['num_tunnels']) + '}'
        _result_ = rt_handle.invoke('multivalue_config',
                                    pattern="counter",
                                    counter_start="1701",
                                    counter_step="1",
                                    counter_direction="increment")
        lac_params['udp_src_port'] = _result_['multivalue_handle']

    result = rt_handle.invoke('l2tp_config', **lac_params)
    if result['status'] == '1':
        rt_handle.lac_handle.append(result['lac_handle'])
        if 'dhcpv6_client_handle' in result:
            rt_handle.dhcpv6_client_handle.append(result['dhcpv6_client_handle'])
            rt_handle.l2tp_client_handle.append(result['dhcpv6_client_handle'])
        else:
            rt_handle.l2tp_client_handle.append(result['pppox_client_handle'])
        rt_handle.pppox_client_handle.append(result['pppox_client_handle'])
    if 'ip_cp' in kwargs:
        if 'ipv6' in kwargs['ip_cp'] or 'dual' in kwargs['ip_cp']:
            dhcp_args = dict()
            dhcp_args['handle'] = result['pppox_client_handle']
            if 'dhcpv6_ia_type' in kwargs:
                dhcp_args['dhcp6_range_ia_type'] = kwargs.get('dhcpv6_ia_type')

            if 'rapid_commit' in kwargs:
                dhcp_args['use_rapid_commit'] = kwargs.get('rapid_commit')

            if 'dhcp4_broadcast' in kwargs:
                dhcp_args['dhcp4_broadcast'] = kwargs.get('dhcp4_broadcast')

            if 'v6_max_no_per_client' in kwargs:
                dhcp_args['dhcp6_range_max_no_per_client'] = kwargs.get('v6_max_no_per_client')

            if 'dhcpv6_iana_count' in kwargs:
                dhcp_args['dhcp6_range_iana_count'] = kwargs.get('dhcpv6_iana_count')

            if 'dhcpv6_iapd_count' in kwargs:
                dhcp_args['dhcp6_range_iapd_count'] = kwargs.get('dhcpv6_iapd_count')
            dhcp_args['dhcp_range_ip_type'] = 'ipv6'
            config_status = rt_handle.invoke('emulation_dhcp_group_config', **dhcp_args)
            if config_status['status'] != '1':
                result['status'] = '0'
                raise Exception("failed to add dhcpv6 client over l2tp")
            else:
                result['dhcpv6_client_handle'] = config_status['dhcpv6client_handle']
                rt_handle.dhcpv6_client_handle.append(result['dhcpv6_client_handle'])
                rt_handle.l2tp_client_handle.append(result['dhcpv6_client_handle'])
    return result


def l2tp_client_action(rt_handle, **kwargs):
    """
    :param rt_handle:                            RT object
    :param kwargs:
    handle:                                 l2tp client handle(pppox client handle/dhcpv6 client handle)
    action:                                 start/stop/abort/restart_down
    :return:                                status dictionary
    """
    param = dict()
    match = re.match(r'\/topology:\d+\/deviceGroup:\d+\/deviceGroup:\d+', kwargs['handle'])
    print(match)
    param['handle'] = match.group(0)
    if 'restart' in kwargs['action']:
        param['action'] = 'restart_down'
    elif 'start' in kwargs['action']:
        param['action'] = 'start_protocol'
    elif 'stop' in kwargs['action']:
        param['action'] = 'stop_protocol'
    elif 'abort' in kwargs['action']:
        param['action'] = 'abort_protocol'
    return rt_handle.invoke('test_control', **param)



def client_action(rt_handle, **kwargs):
    """
    :param rt_handle:                            RT object
    :param kwargs:
    handle:                                 client handle(pppox client handle/dhcp client handle)
    action:                                 start/stop/abort/restart_down
    :return:                                status dictionary
    """
    param = dict()
    match = re.match(r'.*deviceGroup:\d+', kwargs['handle'])
    print(match)
    param['handle'] = match.group(0)
    if 'restart' in kwargs['action']:
        param['action'] = 'restart_down'
    elif 'start' in kwargs['action']:
        param['action'] = 'start_protocol'
    elif 'stop' in kwargs['action']:
        param['action'] = 'stop_protocol'
    elif 'abort' in kwargs['action']:
        param['action'] = 'abort_protocol'
    return rt_handle.invoke('test_control', **param)


def l2tp_client_stats(rt_handle, **kwargs):
    """

    :param rt_handle:
    :param kwargs:
    handle:                             l2tp client handle/ pppox client handle/ dhcpv6 client handle
    mode:                               aggregate/session/tunnel/session_all/session_dhcpv6pd
    :return:
    """
    if 'lac' in kwargs['handle']:
        return rt_handle.invoke('l2tp_stats', **kwargs)
    elif 'dhcpv6' in kwargs['handle']:
        return rt_handle.dhcp_client_stats(handle=kwargs['handle'], mode='aggregate_stats')
    elif 'pppox' in kwargs['handle']:
        return rt_handle.pppoe_client_stats(**kwargs)

def add_dhcp_server(rt_handle, **kwargs):
    """
    :param rt_handle                            RT object
    :param kwargs:
    handle:                                ipv4 handle or ipv6 handle or pppox server handle
    pool_size:                             server pool size
    pool_start_addr:                       pool start address
    pool_mask_length:                      pool prefix length
    pool_gateway:                          pool gateway address
    lease_time                             pool address lease time
    dhcpv6_ia_type:                        v6 IA type "iana, iapd, iana+iapd"
    pool_prefix_start:                     v6 PD start prefix
    pool_prefix_length:                    v6 prefix length
    pool_prefix_size:                      v6 prefix pool size

    # use_rapid_commit                       = "0",
    # subnet_addr_assign                     = "0",
    # subnet                                 = "relay",

    :param kwargs:
    :return:
    a dictionary of status dhcpv4_server_handle dhcpv6_server_handle
    """
    result = dict()
    result['status'] = '1'
    if 'handle' not in kwargs:
        raise Exception("ip handle must be provided ")
    dhcp_args = dict()
    dhcp_args['handle'] = kwargs['handle']
    dhcp_args['mode'] = 'create'
    if 'lease_time' in kwargs:
        dhcp_args['lease_time'] = kwargs['lease_time']
    if 'pool_start_addr' in kwargs:
        dhcp_args['ipaddress_pool'] = kwargs['pool_start_addr']
    if 'pool_mask_length' in kwargs:
        dhcp_args['ipaddress_pool_prefix_length'] = kwargs['pool_mask_length']
    if 'pool_size' in kwargs:
        dhcp_args['ipaddress_count'] = kwargs['pool_size']
    if 'pool_gateway' in kwargs:
        dhcp_args['dhcp_offer_router_address'] = kwargs['pool_gateway']
    if 'v4' in kwargs['handle']:
        dhcp_args['ip_version'] = '4'

    if 'v6' in kwargs['handle'] or 'pppox' in kwargs['handle']:
        dhcp_args['ip_version'] = '6'
        if 'dhcpv6_ia_type' in kwargs:
            dhcp_args['dhcp6_ia_type'] = kwargs['dhcpv6_ia_type']
        if 'pool_prefix_start' in kwargs:
            dhcp_args['start_pool_prefix'] = kwargs['pool_prefix_start']
        if 'pool_prefix_length' in kwargs:
            dhcp_args['prefix_length'] = kwargs['pool_prefix_length']
        if 'pool_prefix_size' in kwargs:
            dhcp_args['pool_prefix_size'] = kwargs['pool_prefix_size']
    config_status = rt_handle.invoke('emulation_dhcp_server_config', **dhcp_args)
    if config_status['status'] != '1':
        result['status'] = '0'
    else:
        if dhcp_args['ip_version'] == "4":
            rt_handle.dhcpv4_server_handle.append(config_status['dhcpv4server_handle'])
            result['dhcpv4_server_handle'] = config_status['dhcpv4server_handle']
        else:
            rt_handle.dhcpv6_server_handle.append(config_status['dhcpv6server_handle'])
            result['dhcpv6_server_handle'] = config_status['dhcpv6server_handle']
    return result


def dhcp_server_action(rt_handle, **kwargs):
    """
    :param rt_handle:                    RT object
    :param kwargs:
     handle:                        dhcp server handle
     action:                        'start' or stop
    :return:
     result
    """
    dhcp_args = dict()
    if 'handle' in kwargs:
        dhcp_args['handle'] = kwargs['handle']
    if 'action' in kwargs:
        if 'start' in kwargs['action']:
            dhcp_args['action'] = 'start_protocol'
        if 'stop' in kwargs['action']:
            dhcp_args['action'] = 'stop_protocol'
    result = rt_handle.invoke('test_control' **dhcp_args)
    return result


def traffic_simulation(rt_handle, **kwargs):
    """
    simulation traffic
    :param rt_handle:
    src_port:                            source port
    dst_port:                            destination port
    encap_pppoe:                         pppoe simulation
    l3_protocol:                         ipv4/ipv6
    l4_protocol:                         icmpv6/dhcp/dhcpv6/udp
    frame_size:                          frame size
    rate_pps:                            rate in pps
    rate_bps:                            rate in bps
    rate_percent:                        rate in percent
    packets_count:                       number of packets to be transmitted
    duration:                            traffic duration
    message_type:                        message type used by icmpv6(echo_req|echo_reply)/dhcp(discover|request|
                                         |release)/dhcpv6(solicit|request|release)/pppoe(PADI/PADR/PADT)
    vlan_id:                             start vlan id
    vlan_step:                           vlan step mode
    vlan_count:                          vlan counts
    svlan_id:                            start svlan id
    svlan_step:                          svlan step mode
    svlan_count:                         svlan counts
    src_mac:                             source mac address
    src_mac_step:                        source mac step
    src_mac_count:                       source mac count
    dst_mac:                             destination mac address
    dst_mac_step:                        destination mac step
    dst_mac_count:                       destination mac count
    src_ip:                              source ipv4 address
    src_ip_step:                         source ipv4 address step
    src_ip_count:                        source ipv4 count
    dst_ip:                              destination ipv4 address
    dst_ip_step:                         destination ipv4 address step
    dst_ip_count:                        destination ipv4 count
    src_ipv6:                            source ipv6 address
    src_ipv6_step:                       source ipv6 address step
    src_ipv6_count:                      source ipv6 address count
    dst_ipv6:                            destination ipv6 address
    dst_ipv6_step:                       destination ipv6 address step
    dst_ipv6_count:                      destination ipv6 address count


    :param kwargs:
    :return: result:                     dictionary of status/stream_id/traffic_item
    """
    traffic_args = dict()
    pppoe_args = dict()
    stack_index = 1
    traffic_args['traffic_generator'] = 'ixnetwork_540'
    traffic_args['circuit_type'] = 'raw'
    traffic_args['track_by'] = 'traffic_item'
    traffic_args['emulation_dst_handle'] = rt_handle.port_to_handle_map[kwargs['dst_port']]
    traffic_args['emulation_src_handle'] = rt_handle.port_to_handle_map[kwargs['src_port']]
    traffic_args['mode'] = 'create'
    traffic_args['frame_size'] = kwargs.get('frame_size', '1000')
    if 'name' in kwargs:
        traffic_args['name'] = kwargs['name']
    if 'rate_pps' in kwargs:
        traffic_args['rate_pps'] = kwargs['rate_pps']
    if 'rate_bps' in kwargs:
        traffic_args['rate_bps'] = kwargs['rate_bps']
    if 'rate_percent' in kwargs:
        traffic_args['rate_percent'] = kwargs['rate_percent']
    if 'packets_count' in kwargs:
        traffic_args['transmit_mode'] = 'single_burst'
        #traffic_args['pkts_per_burst'] = kwargs['packets_count']
        traffic_args['number_of_packets_per_stream'] = kwargs['packets_count']

    if 'duration' in kwargs:
        traffic_args['duration'] = kwargs['duration']

    if 'vlan_id' in kwargs:
        stack_index += 1
        traffic_args['vlan'] = 'enable'
        traffic_args['vlan_id'] = kwargs['vlan_id']
        if 'vlan_step' in kwargs:
            traffic_args['vlan_id_step'] = kwargs['vlan_step']
        if 'vlan_count' in kwargs:
            traffic_args['vlan_id_count'] = kwargs['vlan_count']
        if 'svlan_id' in kwargs:
            stack_index += 1
            traffic_args['vlan_id'] = [kwargs['svlan_id'], kwargs['vlan_id']]
            if 'svlan_step' in kwargs or 'vlan_step' in kwargs:
                traffic_args['vlan_id_step'] = [kwargs.get('svlan_step', 1), kwargs.get('vlan_step', 1)]
            if 'svlan_count' in kwargs or 'vlan_count' in kwargs:
                traffic_args['vlan_id_count'] = [kwargs.get('svlan_count', 1), kwargs.get('vlan_count', 1)]
        if 'vlan_priority' in kwargs:
            traffic_args['vlan_user_priority'] = kwargs['vlan_priority']
        if 'vlan_priority_step' in kwargs:
            traffic_args['vlan_user_priority_step'] = kwargs['vlan_priority_step']
        if 'svlan_priority' in kwargs:
            traffic_args['vlan_user_priority'] = '{},{}'.format(kwargs['svlan_priority'], kwargs['vlan_priority'])
        if 'svlan_priority_step' in kwargs:
            traffic_args['vlan_user_priority_step'] = \
                '{},{}'.format(kwargs['svlan_priority_step'], kwargs['svlan_priority_step'])
    if 'src_mac' in kwargs:
        traffic_args['mac_src'] = kwargs['src_mac']
        if 'src_mac_step' in kwargs:
            traffic_args['mac_src_mode'] = 'increase'
            traffic_args['mac_src_step'] = kwargs['src_mac_step']
            traffic_args['mac_src_count'] = kwargs.get('src_mac_count', 1)
    if 'dst_mac' in kwargs:
        traffic_args['mac_dst'] = kwargs['dst_mac']
        if 'dst_mac_step' in kwargs:
            traffic_args['mac_dst_mode'] = 'increase'
            traffic_args['mac_dst_step'] = kwargs['dst_mac_step']
            traffic_args['mac_dst_count'] = kwargs.get('dst_mac_count', 1)

    if 'src_ip' in kwargs:
        traffic_args['ip_src_addr'] = kwargs['src_ip']
        if 'src_ip_step' in kwargs:
            traffic_args['ip_src_mode'] = 'increase'
            traffic_args['ip_src_step'] = kwargs['src_ip_step']
            traffic_args['ip_src_count'] = kwargs.get('src_ip_count', 1)

    if 'src_ipv6' in kwargs:
        traffic_args['ipv6_src_addr'] = kwargs['src_ipv6']
        if 'src_ipv6_step' in kwargs:
            traffic_args['ipv6_src_mode'] = 'increase'
            traffic_args['ipv6_src_step'] = kwargs['src_ipv6_step']
            traffic_args['ipv6_src_count'] = kwargs.get('src_ipv6_count', 1)

    if 'dst_ip' in kwargs:
        traffic_args['ip_dst_addr'] = kwargs['dst_ip']
        if 'dst_ip_step' in kwargs:
            traffic_args['ip_dst_mode'] = 'increase'
            traffic_args['ip_dst_step'] = kwargs['dst_ip_step']
            traffic_args['ip_dst_count'] = kwargs.get('dst_ip_count', 1)
    if 'dst_ipv6' in kwargs:
        traffic_args['ipv6_dst_addr'] = kwargs['dst_ipv6']

        if 'dst_ipv6_step' in kwargs:
            traffic_args['ipv6_dst_mode'] = 'increase'
            traffic_args['ipv6_dst_step'] = kwargs['dst_ipv6_step']
            traffic_args['ipv6_dst_count'] = kwargs.get('dst_ipv6_count', 1)

    if 'ip_precedence' in kwargs:
        traffic_args['ip_precedence'] = kwargs['ip_precedence']
    if 'ip_precedence_mode' in kwargs:
        traffic_args['ip_precedence_mode'] = kwargs['ip_precedence_mode']
    if 'ip_precedence_step' in kwargs:
        traffic_args['ip_precedence_step'] = kwargs['ip_precedence_step']
    if 'ip_precedence_count' in kwargs:
        traffic_args['ip_precedence_count'] = kwargs['ip_precedence_count']
    if 'ip_dscp' in kwargs:
        traffic_args['ip_dscp'] = kwargs['ip_dscp']
    if 'ip_dscp_mode' in kwargs:
        traffic_args['ip_dscp_mode'] = kwargs['ip_dscp_mode']
    if 'ip_dscp_step' in kwargs:
        traffic_args['ip_dscp_step'] = kwargs['ip_dscp_step']
    if 'ip_dscp_count' in kwargs:
        traffic_args['ip_dscp_count'] = kwargs['ip_dscp_count']

    if 'l4_protocol' in kwargs:
        traffic_args['l4_protocol'] = kwargs['l4_protocol']
        if 'dhcpv6' in kwargs['l4_protocol']:
            traffic_args['l4_protocol'] = 'udp'

    if 'l3_protocol' in kwargs:
        traffic_args['l3_protocol'] = kwargs['l3_protocol']

    result = rt_handle.invoke('traffic_config', **traffic_args)
    if result['status'] != '1':
        return result
    else:
        rt_handle.traffic_item.append(result['stream_id'])
        traffic_handle = result['stream_id']
        stream_id = result['traffic_item']
        headers = result[stream_id]['headers'].split(' ')
        index = stack_index - 1
        pppoe_header = headers[index]
        pppoe_index = stack_index

    if 'l4_protocol' in kwargs and kwargs['l4_protocol'] == 'dhcp':
        ##enable dhcp option
        header_handle = headers[-2]
        message_handle = 'dhcp.header.options.fields.nextOption.field.dhcpMessageType.code-182'
        option_args = dict()
        option_args['mode'] = 'set_field_values'
        option_args['traffic_generator'] = 'ixnetwork_540'
        option_args['header_handle'] = header_handle
        option_args['field_handle'] = message_handle
        option_args['field_optionalEnabled'] = '1'
        rt_handle.invoke('traffic_config', **option_args)
        ## set dhcp message type
        dhcp_msg = 'DHCP' + kwargs['message_type'].upper()
        msg_args = dict()
        msg_args['mode'] = 'set_field_values'
        message_handle = 'dhcp.header.options.fields.nextOption.field.dhcpMessageType.messageType-184'
        msg_args['traffic_generator'] = 'ixnetwork_540'
        msg_args['field_handle'] = message_handle
        msg_args['header_handle'] = header_handle
        msg_args['field_fieldValue'] = dhcp_msg
        result = rt_handle.invoke('traffic_config', **msg_args)
        return result

    if 'l4_protocol' in kwargs and 'dhcpv6' in kwargs['l4_protocol']:
        stack_index += 3  ##ipv6 layer + udp layer + dhcpv6 layer
        dhcpv6_args = dict()
        dhcpv6_args['traffic_generator'] = 'ixnetwork_540'
        dhcpv6_args['mode'] = 'modify_or_insert'
        dhcpv6_args['stream_id'] = stream_id
        dhcpv6_args['stack_index'] = stack_index
        dhcpv6_args['pt_handle'] = 'dhcpv6ClientServer'
        ## this is dhcpv6 solicit by default
        result = rt_handle.invoke('traffic_config', **dhcpv6_args)
        if result['status'] != '1':
            traffic_action(rt_handle, handle=traffic_handle, action='delete')
            return result
        else:
            headers = result[stream_id]['headers'].split(' ')
            field_handle = result['traffic_item']
            if 'message_type' in kwargs:
                msg_type = kwargs['message_type'].capitalize()
                v6_msg_args = dict()
                v6_msg_args['mode'] = 'set_field_values'
                v6_msg_args['header_handle'] = headers[-2]
                v6_msg_args['field_handle'] = 'dhcpv6ClientServer.header.messageType-1'
                v6_msg_args['field_fieldValue'] = msg_type
                rt_handle.invoke('traffic_config', **v6_msg_args)

    if 'l4_protocol' in kwargs and 'icmpv6' in kwargs['l4_protocol']:

        if 'message_type' in kwargs and 'echo_req' in kwargs['message_type']:
            field_handle = "icmpv6.icmpv6Message.icmpv6MessegeType.echoRequestMessage.messageType-17"
        if 'message_type' in kwargs and 'echo_reply' in kwargs['message_type']:
            field_handle = "icmpv6.icmpv6Message.icmpv6MessegeType.echoReplyMessage.messageType-22"
        if 'message_type' in kwargs:
            result = rt_handle.invoke('traffic_config',
                                      mode='set_field_values',
                                      traffic_generator='ixnetwork_540',
                                      pt_handle='icmpv6',
                                      header_handle=headers[-2],
                                      field_handle=field_handle,
                                      field_activeFieldChoice='1')
        if result['status'] != '1':
            traffic_action(rt_handle, handle=result['stream_id'], action='delete')
            return result

    if 'pppoe_encap' in kwargs and kwargs['pppoe_encap']:
        pppoe_args = dict()
        pppoe_args['traffic_generator'] = 'ixnetwork_540'
        pppoe_args['mode'] = 'append_header'
        pppoe_args['stream_id'] = pppoe_header
        pppoe_args['stack_index'] = pppoe_index
        if 'message_type' in kwargs:
            pppoe_args['pt_handle'] = 'pppoEDiscovery'
        else:
            pppoe_args['pt_handle'] = 'pppoESession'
        result = rt_handle.invoke('traffic_config', **pppoe_args)
        if result['status'] != '1':
            traffic_action(rt_handle, handle=result['stream_id'], action='delete')
            return result
        headers = result['handle']

        if 'message_type' in kwargs:
            pppoe_msg_type = kwargs['message_type'].upper()
            pppoe_msg_args = dict()
            pppoe_msg_args['mode'] = 'set_field_values'
            pppoe_msg_args['header_handle'] = headers
            pppoe_msg_args['field_handle'] = 'pppoEDiscovery.header.header.opcode-3'
            pppoe_msg_args['field_fieldValue'] = pppoe_msg_type
            rt_handle.invoke('traffic_config', **pppoe_msg_args)

    return result


def ancp_action(rt_handle, **kwargs):
    """

    :param rt_handle:
    :param kwargs:
    handle:             ancp_handle
    action:             start/stop/abort
    :return:
    """
    handle = kwargs['handle']
    action = kwargs['action']
    if 'start' in action:
        action = 'enable'
    elif 'stop' in action:
        action = 'disable'
    return rt_handle.invoke('emulation_ancp_control', ancp_handle=handle, action=action)


def add_ancp(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    count:                      ANCP node count
    lines_per_node:             subscriber lines per ancp node
    dut_ip:                     router loopback ip address
    port                        physical tester port
    vlan_start:                 ancp vlan id
    vlan_step:                  ancp vlan increase step
    vlan_repeat:                ancp vlan repeat number
    vlan_length:                ancp vlan sequence length
    svlan_start:                ancp svlan id
    svlan_step:                 ancp svlan increase step
    svlan_repeat:               ancp svlan repeat number
    svlan_length:               ancp svlan sequence length
    ip_addr:                    ip address
    ip_addr_step:               ip address step
    mask:                       mask
    gateway:                    gateway address
    ancp_standard:              ancp standard (rfc6320/ietf-ancp-protocol5), by default rfc6320
    keep_alive:                 ancp keepalive timer
    keep_alive_retries:         ancp keepalive retries, by default is 3
    remote_loopback:            1/0
    dsl_type:                   adsl1 adsl2 adsl2_plus vdsl1 vdsl2 sdsl unknown
    vlan_allocation_model:      1_1 or N_1 or disabled, by default is 1_1
    flap_mode:                  none/reset/resynchronize, default is none
    remote_id:                  remote_id string, for example can be "remoteid" or "remoteid?"
    remote_id_start:            remote_id start value
    remote_id_step:             remote_id step value
    remote_id_repeat:           remote_id repeat value
    remote_id_length:           remote_id length
    circuit_id:                 agent_circuit_id string
    circuit_id_start:           circuit_id start value
    circuit_id_step:            circuit_id step value
    circuit_id_repeat:          circuit_id repeat value
    circuit_id_length:          circuit_id length
    enable_remote_id:           1/0, by default is 0
    customer_vlan_start:        the first customer vlan id
    customer_vlan_step:         vlan increase step
    customer_vlan_repeat:       vlan repeat number
    customer_vlan_length:       vlan sequence length
    service_vlan_start:         first service vlan id
    service_vlan_step:          service vlan increase step
    service_vlan_repeat:        svlan repeat number
    service_vlan_length:        svlan sequence length
    :return:                    dictionary of handles: ancp_handle, ancp_subscriber_lines_handle, status
    """
    result = add_link(rt_handle, **kwargs)
    if result['status'] != '1':
        return result
    ip_handle = result['ipv4_handle']
    device_handle = result['device_group_handle']
    ancp_args = dict()
    ancp_args['handle'] = ip_handle
    ancp_args['ancp_standard'] = kwargs.get('ancp_standard', 'rfc6320')
    ancp_args['trigger_access_loop_events'] = '1'
    ancp_args['mode'] = 'create'
    ancp_args['line_config'] = '1'
    ancp_args['topology_discovery'] = '1'
    if 'keep_alive' in kwargs:
        ancp_args['line_config'] = kwargs['line_config']
    if 'keep_alive_retires' in kwargs:
        ancp_args['keep_alive_retires'] = kwargs['keep_alive_retries']
    ancp_args['sut_ip_addr'] = kwargs.get('dut_ip', '100.0.0.1')
    ancp_args['sut_service_port'] = '6068'
    ancp_args['remote_loopback'] = kwargs.get('remote_loopback', '0')
    _result_ = rt_handle.invoke('emulation_ancp_config', **ancp_args)
    if _result_['status'] != '1':
        return _result_
    ancp_handle = _result_['ancp_handle']
    rt_handle.ancp_handle.append(ancp_handle)
    ##create network group first since using ancp_client_handle directly does not work
    network_args = dict()
    network_args['protocol_handle'] = device_handle
    network_args['multiplier'] = kwargs.get('lines_per_node', '1')
    network_args['enable_device'] = "1"
    _result_ = rt_handle.invoke('network_group_config', **network_args)
    if _result_['status'] != '1':
        return _result_

    networkgroup_handle = _result_['network_group_handle']
    line_args = dict()
    line_args['mode'] = 'create'
    line_args['handle'] = networkgroup_handle
    #line_args['ancp_client_handle'] = ancp_handle
    #line_args['subscriber_lines_per_access_node'] = kwargs.get('lines_per_node', '1')
    line_args['dsl_type'] = kwargs.get('dsl_type', "adsl1")
    line_args['enable_dsl_type'] = '1'
    line_args['vlan_allocation_model'] = kwargs.get('vlan_allocation_model', '1_1')
    if 'flap_mode' in kwargs:
        line_args['flap_mode'] = kwargs['flap_mode']
    if 'circuit_id' in kwargs:
        circuit_id = kwargs['circuit_id']
        if 'circuit_id_start' in kwargs:
            start = str(kwargs.get('circuit_id_start'))
            step = str(kwargs.get('circuit_id_step', '1'))
            repeat = str(kwargs.get('circuit_id_repeat', '1'))
            length = str(kwargs.get('circuit_id_length', ''))
            if '?' in circuit_id:
                increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                circuit_id = circuit_id.replace('?', increment)
            else:
                circuit_id = circuit_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
        _result_ = add_string_mv(rt_handle, string_name=circuit_id)
        print(_result_)
        multivalue_6_handle = _result_['multivalue_handle']
        line_args['circuit_id'] = multivalue_6_handle
    if 'remote_id' in kwargs:
        remote_id = kwargs.get('remote_id')
        if 'remote_id_start' in kwargs:
            start = str(kwargs.get('remote_id_start'))
            step = str(kwargs.get('remote_id_step', '1'))
            repeat = str(kwargs.get('remote_id_repeat', '1'))
            length = str(kwargs.get('remote_id_length', ''))
            if '?' in remote_id:
                increment = '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'
                remote_id = remote_id.replace('?', increment)
            else:
                remote_id = remote_id + '{Inc:' + start + ',' + step + ',' + length + ',' + repeat + '}'

        _result_ = add_string_mv(rt_handle, string_name=remote_id)
        print(_result_)
        multivalue_7_handle = _result_['multivalue_handle']
        line_args['remote_id'] = multivalue_7_handle
        line_args['enable_remote_id'] = '1'

    vlan_args = dict()
    if 'service_vlan_start' in kwargs:
        vlan_args['start'] = kwargs.get('service_vlan_start')
        vlan_args['step'] = kwargs.get('service_vlan_step', '1')
        vlan_args['repeat'] = kwargs.get('service_vlan_repeat', '1')
        vlan_args['count'] = kwargs.get('service_vlan_length', '4094')
        line_args['service_vlan_id'] = __set_custom_pattern(rt_handle, **vlan_args)
    if 'customer_vlan_start' in kwargs:
        vlan_args['start'] = kwargs.get('customer_vlan_start')
        vlan_args['step'] = kwargs.get('customer_vlan_step', '1')
        vlan_args['repeat'] = kwargs.get('customer_vlan_repeat', '1')
        vlan_args['count'] = kwargs.get('customer_vlan_length', '4094')
        line_args['customer_vlan_id'] = __set_custom_pattern(rt_handle, **vlan_args)
    line_args['enable_actual_rate_upstream'] = 'true'
    line_args['enable_actual_rate_downstream'] = 'true'

    result = rt_handle.invoke('emulation_ancp_subscriber_lines_config', **line_args)
    if result['status'] != '1':
        return result
    result['ancp_handle'] = ancp_handle
    rt_handle.ancp_line_handle.append(result['ancp_subscriber_lines_handle'])
    return result


def add_lacp(rt_handle, **kwargs):
    """

    :param rt_handle:
    :param kwargs:
    port_list(mandatory):                              ports in AE
    name(mandatory):                                   LAG AE name
    protocol:                               lacp or statisLag
    mode:                                   active or passive
    admin_key:                              administrative key
    actor_system_id:                        AE system id
    actor_key:                              AE actor key
    actor_port_pri:                         AE port priority
    :return:
    """
    rt_handle.ae['handle'] = []
    rt_handle.ae[kwargs['name']] = []
    #rt_handle.ae[kwargs['name']] = kwargs['port_list']
    for port in kwargs['port_list']:
        rt_handle.ae[port] = dict()
        rt_handle.ae[kwargs['name']].append(port)
    result = add_topology(rt_handle, port_list=kwargs['port_list'])
    if result['status'] != "1":
        raise Exception("failed at add topology for ports {}".format(kwargs['port_list']))
    topo_handle = result['topology_handle']
    dev_args = dict()
    dev_args['topology_handle'] = topo_handle
    if 'name' in kwargs:
        dev_args['name'] = kwargs['name']
    result = add_device_group(rt_handle, **dev_args)
    if result['status'] != "1":
        raise Exception("failed at add device for ports {}".format(kwargs['port_list']))
    device_handle = result['device_group_handle']
    result = set_vlan(rt_handle, handle=device_handle)
    ethernet_handle = result['ethernet_handle']
    lacp_args = dict()
    lacp_args['mode'] = "create"
    lacp_args['handle'] = ethernet_handle
    if 'protocol' in kwargs:
        lacp_args['session_type'] = kwargs['protocol']
    if 'actor_key' in kwargs:
        lacp_args['actor_key'] = kwargs['actor_key']
    if 'actor_port_num' in kwargs:
        lacp_args['actor_port_num'] = kwargs['actor_port_num']
    if 'admin_key' in kwargs:
        lacp_args['administrative_key'] = kwargs['admin_key']
    if 'actor_system_id' in kwargs:
        lacp_args['actor_system_id'] = kwargs['actor_system_id']
    if 'mode' in kwargs:
        lacp_args['lacp_activity'] = kwargs['mode']
    if 'actor_port_pri' in kwargs:
        lacp_args['actor_port_pri'] = kwargs['actor_port_pri']

    result = rt_handle.invoke('emulation_lacp_link_config', **lacp_args)
    if result['status'] != "1":
        raise Exception("failed to add lacp for ports {}".format(kwargs['port_list']))
    if 'lacp_handle' in result:
        lacp_handle = result['lacp_handle']
    elif 'staticLag_handle' in result:
        lacp_handle = result['staticLag_handle']
    rt_handle.ae['handle'].append(lacp_handle)
    for port in kwargs['port_list']:
        rt_handle.ae[port]['device_group_handle'] = device_handle
        if 'name' in kwargs:
            rt_handle.ae[port]['ae_name'] = kwargs['name']
        rt_handle.ae[port]['lacp_handle'] = lacp_handle

    return result


def ancp_line_action(rt_handle, **kwargs):
    """
    :param rt_handle:
    :param kwargs:
    handle:             ancp_handle
    action:             flap_start/flat_stop/flap_start_resync/port_up/port_down
    :return:
    """
    sub_handle = kwargs['handle']
    action = kwargs['action']
    if 'flap_start' in action:
        action = 'flap_start'
    elif 'flap_stop' in action:
        action = 'flap_stop'
    elif 'flap_start_resync' in action:
        action = 'flap_start_resync'
    elif 'port_up' in action:
        action = 'send_port_up'
    elif 'port_down' in action:
        action = 'send_port_down'

    return rt_handle.invoke('emulation_ancp_control', ancp_subscriber=sub_handle, action=action)
